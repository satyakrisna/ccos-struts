package CCOS;

public class CCOSException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected int code;
	protected String msg;
	
	public CCOSException(int code) {
	      this.code=code;
	      if(code == 1){
	    	  this.msg="Data already exists!";
	      }else if(code == 1400){
	    	  this.msg="Cannot insert null value!";
	      }else if(code == 2291){
	    	  this.msg="Database error, parent not found!";
	      }else if(code == 2292){
	    	  this.msg="Database error, child found!";
	      }else{
	    	  this.msg="Database error!";
	      }
	}
	
	public String getErrorMsg(){
		return msg;
	}
	
	public String getErrorCode(){
		return Integer.toString(code);
	}
}
