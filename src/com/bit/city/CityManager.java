package com.bit.city;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.bit.province.Province;

import CCOS.CCOSException;
import oracle.jdbc.OracleTypes;

public class CityManager {
	
	private static DataSource ds = null;

	public CityManager() {
		this.SetupDataSource();
	}

	public void SetupDataSource() {

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		env.put(Context.PROVIDER_URL, "t3://localhost:7001");
		try {
			Context context = new InitialContext(env);
			ds = (DataSource) context.lookup("ds01");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ArrayList<City> getAllCity() throws Exception {

		ArrayList<City> cityList = new ArrayList<City>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_CITY(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				City city = new City();
				city.setCode(rs.getString("CODE"));
				city.setDescription(rs.getString("DESCRIPTION"));
				city.setProvince(rs.getString("PROVINCE"));
				city.setDate_created(rs.getDate("DATE_CREATED"));
				city.setCreated_by(rs.getString("CREATED_BY"));
				city.setDate_updated(rs.getDate("DATE_UPDATED"));
				city.setUpdated_by(rs.getString("UPDATED_BY"));
				cityList.add(city);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return cityList;
	}

	public ArrayList<City> getCityByCode(String pcode) throws Exception {

		ArrayList<City> cityList = new ArrayList<City>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_CITY_BY_CODE(?,?)}");
			cs.setString(1, pcode);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				City city = new City();
				city.setCode(rs.getString("CODE"));
				city.setDescription(rs.getString("DESCRIPTION"));
				city.setProvince(rs.getString("PROVINCE"));
				city.setDate_created(rs.getDate("DATE_CREATED"));
				city.setCreated_by(rs.getString("CREATED_BY"));
				city.setDate_updated(rs.getDate("DATE_UPDATED"));
				city.setUpdated_by(rs.getString("UPDATED_BY"));
				cityList.add(city);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return cityList;
	}

	public ArrayList<City> getCityByDesc(String desc) throws Exception {

		ArrayList<City> cityList = new ArrayList<City>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_CITY_BY_DESC(?,?)}");
			cs.setString(1, desc);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				City city = new City();
				city.setCode(rs.getString("CODE"));
				city.setDescription(rs.getString("DESCRIPTION"));
				city.setProvince(rs.getString("PROVINCE"));
				city.setDate_created(rs.getDate("DATE_CREATED"));
				city.setCreated_by(rs.getString("CREATED_BY"));
				city.setDate_updated(rs.getDate("DATE_UPDATED"));
				city.setUpdated_by(rs.getString("UPDATED_BY"));
				cityList.add(city);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return cityList;
	}

	public void addCity(String code, String p_code, String desc, String created_by) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("CALL ADD_CITY(?,?,?,?)");
			cs.setString(1, code);
			cs.setString(2, p_code);
			cs.setString(3, desc);
			cs.setString(4, created_by);
			cs.execute();
			System.out.println("Data added");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new CCOSException(e.getErrorCode());
		}finally {
			cs.close();
			conn.close();
		}
	}

	public City getCityDetail(String code) throws Exception {

		City city = new City();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_CITY_DETAIL(?,?)}");
			cs.setString(1, code);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				city.setCode(rs.getString("CODE"));
				city.setP_code(rs.getString("P_CODE"));
				city.setDescription(rs.getString("DESCRIPTION"));
				city.setProvince(rs.getString("PROVINCE"));
				city.setDate_created(rs.getDate("DATE_CREATED"));
				city.setCreated_by(rs.getString("CREATED_BY"));
				city.setDate_updated(rs.getDate("DATE_UPDATED"));
				city.setUpdated_by(rs.getString("UPDATED_BY"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return city;
	}

	public void updateCity(String code, String p_code, String desc, String updated_by) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL UPDATE_CITY(?,?,?,?)}");
			cs.setString(1, code);
			cs.setString(2, p_code);
			cs.setString(3, desc);
			cs.setString(4, updated_by);
			cs.execute();
			System.out.println("Data updated");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
	}

	public void deleteCity(String code) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("CALL DELETE_CITY(?)");
			cs.setString(1, code);
			cs.execute();
			System.out.println("Data deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
	}
	
	public ArrayList<Province> getAllProvince() throws Exception {

		ArrayList<Province> provList = new ArrayList<Province>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_PROVINCE(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				Province prov = new Province();
				prov.setCode(rs.getString("CODE"));
				prov.setDescription(rs.getString("DESCRIPTION"));
				prov.setDate_created(rs.getDate("DATE_CREATED"));
				prov.setCreated_by(rs.getString("CREATED_BY"));
				prov.setDate_updated(rs.getDate("DATE_UPDATED"));
				prov.setUpdated_by(rs.getString("UPDATED_BY"));

				provList.add(prov);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return provList;
	}
}
