package com.bit.city;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import CCOS.CCOSException;

public class CityController extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if(request.getSession().getAttribute("is_login")!=null){
			CityForm cf = (CityForm) form;
			CityManager cm = new CityManager();
			City c = new City();
			HttpSession session=request.getSession();
			
			String task = cf.getTask();
			String search = cf.getSearch();
			String value = cf.getValue();
			String code = cf.getCode();
			String p_code = cf.getP_code();
			String desc = cf.getDescription();
			String username = (String)session.getAttribute("username");
			String map = "";

			if (task == null||task.equals("load")) {
				cf.setCityList(cm.getAllCity());
				map = "list";
			} else if (task.equals("search") && search.equals("code")) {
				cf.setCityList(cm.getCityByCode(value));
				map = "search";
			} else if (task.equals("search") && search.equals("description")) {
				cf.setCityList(cm.getCityByDesc(value));
				map = "search";
			} else if (task.equals("add")) {
				cf.setProvList(cm.getAllProvince());
				resetCityForm(cf);
				map = "add";
			} else if (task.equals("save")) {
				try{
					cm.addCity(code, p_code, desc, username);
					request.setAttribute("update_msg", "Data updated.");
					c=cm.getCityDetail(code);
					setCityForm(cf, c);
					map = "detail";
				}catch(CCOSException e){
					request.setAttribute("error", "ERR["+e.getErrorCode()+"] "+e.getErrorMsg());
					map = "add";
				}
			} else if (task.equals("detail")) {
				c=cm.getCityDetail(code);
				setCityForm(cf, c);
				map = "detail";
			} else if (task.equals("edit")) {
				cf.setProvList(cm.getAllProvince());
				c=cm.getCityDetail(code);
				setCityForm(cf, c);
				map = "edit";
			} else if (task.equals("update")) {
				cm.updateCity(code, p_code, desc, username);
				request.setAttribute("update_msg", "Data updated.");
				c=cm.getCityDetail(code);
				setCityForm(cf, c);
				map = "detail";
			} else if (task.equals("delete")) {
				cm.deleteCity(code);
				request.setAttribute("delete_msg", "Data deleted.");
				map = "list";
			} else if (task.equals("logout")) {
				session.invalidate();
				map="login";
			}
			
			if(map.equals("list")){
				resetCityForm(cf);
				cf.setCityList(cm.getAllCity());
			}
			return mapping.findForward(map);
		}else{
			return mapping.findForward("login");
		}
	}
	
	public CityForm setCityForm(CityForm cf, City c){
		cf.setCode(c.getCode());
		cf.setP_code(c.getP_code());
		cf.setProvince(c.getProvince());
		cf.setDescription(c.getDescription());
		cf.setDate_created(c.getDate_created().toString());
		cf.setCreated_by(c.getCreated_by());
		if(c.getDate_updated()!=null){
			cf.setDate_updated(c.getDate_updated().toString());
		}else{
			cf.setDate_updated("");
		}
		if(c.getUpdated_by()!=null){
			cf.setUpdated_by(c.getUpdated_by());
		}else{
			cf.setUpdated_by("");
		}
		return cf;
	}
	
	public CityForm resetCityForm(CityForm cf){
		cf.setCode("");
		cf.setP_code("");
		cf.setProvince("");
		cf.setDescription("");
		cf.setDate_created("");
		cf.setCreated_by("");
		cf.setDate_updated("");
		cf.setUpdated_by("");
		cf.setSearch("code");
		cf.setValue("");
		return cf;
	}
}
