package com.bit.city;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.bit.province.Province;

public class CityForm extends ActionForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String p_code;
	private String province;
	private String description;
	private String date_created;
	private String created_by;
	private String date_updated;
	private String updated_by;
	private String task;
	private String search="code";
	private String value;
	
	private ArrayList<City> cityList;
	private ArrayList<Province> provList;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getP_code() {
		return p_code;
	}
	public void setP_code(String p_code) {
		this.p_code = p_code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate_created() {
		return date_created;
	}
	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getDate_updated() {
		return date_updated;
	}
	public void setDate_updated(String date_updated) {
		this.date_updated = date_updated;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public ArrayList<City> getCityList() {
		return cityList;
	}
	public void setCityList(ArrayList<City> cityList) {
		this.cityList = cityList;
	}
	public ArrayList<Province> getProvList() {
		return provList;
	}
	public void setProvList(ArrayList<Province> provList) {
		this.provList = provList;
	}
	
	

}
