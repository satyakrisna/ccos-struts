package com.bit.home;

import org.apache.struts.action.ActionForm;

public class HomeForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String task;
	
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}

}
