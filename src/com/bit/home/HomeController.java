package com.bit.home;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class HomeController extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		if (request.getSession().getAttribute("is_login") != null) {
			
			HomeForm hf = (HomeForm) form;
			if(hf.getTask().equals("process")){
				hf.setTask("load");
			}
			String task = hf.getTask();
			
			HttpSession session = request.getSession();
			String role = (String)session.getAttribute("role");
			String map = "";
			
			if(task==null||task.equals("load")){
				if (role.equals("Admin")) {
					map= "home_admin";
				} else {
					map="home_operator";
				}
			} else if (task.equals("logout")){
				session.invalidate();
				map="login";
			}
			return mapping.findForward(map);
		} else {
			return mapping.findForward("login");
		}
	}
}
