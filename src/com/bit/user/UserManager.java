package com.bit.user;

import java.security.MessageDigest;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import CCOS.CCOSException;
import oracle.jdbc.OracleTypes;

public class UserManager {

	private static DataSource ds = null;
	
	public UserManager(){
		this.SetupDataSource();
	}
	
	public void SetupDataSource() {

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		env.put(Context.PROVIDER_URL, "t3://localhost:7001");
		try {
			Context context = new InitialContext(env);
			ds = (DataSource) context.lookup("ds01");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public ArrayList<User> getAllUser() throws Exception {

		ArrayList<User> userList = new ArrayList<User>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_USER(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				User user = new User();
				user.setUser_id(rs.getString("USER_ID"));
				user.setUsername(rs.getString("USERNAME"));
				if(rs.getInt("ROLE")==1){
					user.setRole("Admin");
				}else{
					user.setRole("Operator");
				}
				user.setPassword(rs.getString("PASSWORD"));
				user.setDate_created(rs.getDate("DATE_CREATED"));
				user.setCreated_by(rs.getString("CREATED_BY"));
				user.setDate_updated(rs.getDate("DATE_UPDATED"));
				user.setUpdated_by(rs.getString("UPDATED_BY"));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return userList;
	}

	public ArrayList<User> getUserById(String uid) throws Exception {

		ArrayList<User> userList = new ArrayList<User>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_USER_BY_ID(?,?)}");
			cs.setString(1, uid);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				User user = new User();
				user.setUser_id(rs.getString("USER_ID"));
				user.setUsername(rs.getString("USERNAME"));
				if(rs.getInt("ROLE")==1){
					user.setRole("Admin");
				}else{
					user.setRole("Operator");
				}
				user.setPassword(rs.getString("PASSWORD"));
				user.setDate_created(rs.getDate("DATE_CREATED"));
				user.setCreated_by(rs.getString("CREATED_BY"));
				user.setDate_updated(rs.getDate("DATE_UPDATED"));
				user.setUpdated_by(rs.getString("UPDATED_BY"));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return userList;
	}

	public ArrayList<User> getUserByName(String name) throws Exception {

		ArrayList<User> userList = new ArrayList<User>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_USER_BY_NAME(?,?)}");
			cs.setString(1, name);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				User user = new User();
				user.setUser_id(rs.getString("USER_ID"));
				user.setUsername(rs.getString("USERNAME"));
				if(rs.getInt("ROLE")==1){
					user.setRole("Admin");
				}else{
					user.setRole("Operator");
				}
				user.setPassword(rs.getString("PASSWORD"));
				user.setDate_created(rs.getDate("DATE_CREATED"));
				user.setCreated_by(rs.getString("CREATED_BY"));
				user.setDate_updated(rs.getDate("DATE_UPDATED"));
				user.setUpdated_by(rs.getString("UPDATED_BY"));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return userList;
	}

	public void addUser(String uid, String uname, String role, String password, String created_by) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;
		password=this.getSecurePassword(password);

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("CALL ADD_USER(?,?,?,?,?)");
			cs.setString(1, uid);
			cs.setString(2, uname);
			if(role.equals("Admin")){
				cs.setInt(3, 1);
			}else{
				cs.setInt(3, 2);
			}
			cs.setString(4, password);
			cs.setString(5, created_by);
			cs.execute();
			System.out.println("Data added");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new CCOSException(e.getErrorCode());
		}finally {
			cs.close();
			conn.close();
		}
	}

	public User getUserDetail(String uid) throws Exception {

		User user = new User();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_USER_DETAIL(?,?)}");
			cs.setString(1, uid);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				user.setUser_id(rs.getString("USER_ID"));
				user.setUsername(rs.getString("USERNAME"));
				if(rs.getInt("ROLE")==1){
					user.setRole("Admin");
				}else{
					user.setRole("Operator");
				}
				user.setPassword(rs.getString("PASSWORD"));
				user.setDate_created(rs.getDate("DATE_CREATED"));
				user.setCreated_by(rs.getString("CREATED_BY"));
				user.setDate_updated(rs.getDate("DATE_UPDATED"));
				user.setUpdated_by(rs.getString("UPDATED_BY"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return user;
	}

	public void updateUser(String uid, String uname, String role, String password, String updated_by) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;
		password=this.getSecurePassword(password);

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL UPDATE_USER(?,?,?,?,?)}");
			cs.setString(1, uid);
			cs.setString(2, uname);
			if(role.equals("Admin")){
				cs.setInt(3, 1);
			}else{
				cs.setInt(3, 2);
			}
			cs.setString(4, password);
			cs.setString(5, updated_by);
			cs.execute();
			System.out.println("Data updated");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
	}

	public void deleteUser(String uid) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("CALL DELETE_USER(?)");
			cs.setString(1, uid);
			cs.execute();
			System.out.println("Data deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
	}
	
	private String getSecurePassword(String password)
    {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes 
            
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
	
}
