package com.bit.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import CCOS.CCOSException;

public class UserController extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if(request.getSession().getAttribute("is_login")!=null){
			UserForm uf = (UserForm) form;
			UserManager um = new UserManager();
			User u = new User();
			HttpSession session=request.getSession();
			
			String task = uf.getTask();
			String search = uf.getSearch();
			String value = uf.getValue();
			String uid = uf.getUser_id();
			String name = uf.getUsername();
			String role = uf.getRole();
			String pass = uf.getPassword();
			String username = (String)session.getAttribute("username");
			String map = "";
			
			if (task == null||task.equals("load")) {
				uf.setUserList(um.getAllUser());
				map = "list";
			} else if (task.equals("search") && search.equals("id")) {
				uf.setUserList(um.getUserById(value));
				map = "search";
			} else if (task.equals("search") && search.equals("name")) {
				uf.setUserList(um.getUserByName(value));
				map = "search";
			} else if (task.equals("add")) {
				resetUserForm(uf);
				map = "add";
			} else if (task.equals("save")) {
				try{
					um.addUser(uid, name, role, pass, username);
					request.setAttribute("add_msg", "Data added.");
					u=um.getUserDetail(uid);
					setUserForm(uf, u);
					map = "detail";
				}catch(CCOSException e){
					request.setAttribute("error", "ERR["+e.getErrorCode()+"] "+e.getErrorMsg());
					map = "add";
				}
			} else if (task.equals("detail")) {
				u=um.getUserDetail(uid);
				setUserForm(uf, u);
				map = "detail";
			} else if (task.equals("edit")) {
				u=um.getUserDetail(uid);
				setUserForm(uf, u);
				map = "edit";
			} else if (task.equals("update")) {
				um.updateUser(uid, name, role, pass, username);
				request.setAttribute("update_msg", "Data updated.");
				u=um.getUserDetail(uid);
				setUserForm(uf, u);
				map = "detail";
			} else if (task.equals("delete")) {
				um.deleteUser(uid);
				request.setAttribute("delete_msg", "Data deleted.");
				map = "list";
			} else if (task.equals("logout")) {
				session.invalidate();
				map="login";
			}
			
			if(map.equals("list")){
				resetUserForm(uf);
				uf.setUserList(um.getAllUser());
			}
			return mapping.findForward(map);
		}else{
			return mapping.findForward("login");
		}
	}

	public UserForm setUserForm(UserForm uf, User u) {
		uf.setUser_id(u.getUser_id());
		uf.setUsername(u.getUsername());
		uf.setRole(u.getRole());
		uf.setPassword(u.getPassword());
		uf.setDate_created(u.getDate_created().toString());
		uf.setCreated_by(u.getCreated_by());
		if (u.getDate_updated() != null) {
			uf.setDate_updated(u.getDate_updated().toString());
		} else {
			uf.setDate_updated("");
		}
		if (u.getUpdated_by() != null) {
			uf.setUpdated_by(u.getUpdated_by());
		} else {
			uf.setUpdated_by("");
		}
		return uf;
	}

	public UserForm resetUserForm(UserForm uf) {
		uf.setUser_id("");
		uf.setUsername("");
		uf.setRole("Admin");
		uf.setPassword("");
		uf.setDate_created("");
		uf.setCreated_by("");
		uf.setDate_updated("");
		uf.setUpdated_by("");
		uf.setSearch("id");
		uf.setValue("");
		
		return uf;
	}
}
