package com.bit.customer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.bit.city.City;
import com.bit.occupation.Occupation;
import com.bit.province.Province;

import CCOS.CCOSException;
import oracle.jdbc.OracleTypes;

public class CustomerManager {

private static DataSource ds = null;
	
	public CustomerManager(){
		this.SetupDataSource();
	}
	
	public void SetupDataSource() {

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		env.put(Context.PROVIDER_URL, "t3://localhost:7001");
		try {
			Context context = new InitialContext(env);
			ds = (DataSource) context.lookup("ds01");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public ArrayList<Customer> getAllCustomer() throws Exception {

		ArrayList<Customer> custList = new ArrayList<Customer>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_CUSTOMER(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				Customer cust = new Customer();
				cust.setId_number(rs.getString("ID_NUMBER"));
				cust.setName(rs.getString("NAME"));
				cust.setFullname(rs.getString("FULLNAME"));
				cust.setId_type(rs.getString("ID_TYPE"));
				cust.setId_exp_date(rs.getDate("ID_EXP_DATE").toString());
				cust.setGender(rs.getString("GENDER"));
				cust.setPob(rs.getString("PLACE_OF_BIRTH"));
				cust.setCob(rs.getString("CITY"));
				cust.setProb(rs.getString("PROVINCE"));
				cust.setDob(rs.getDate("DATE_OF_BIRTH").toString());
				cust.setM_status(rs.getString("MARITAL_STATUS"));
				cust.setNo_dep(rs.getInt("NO_DEPENDENT"));
				cust.setEdu_level(rs.getString("EDUCATION_LEVEL"));
				cust.setMother_name(rs.getString("MOTHER_NAME"));
				cust.setR_address_1(rs.getString("ADDRESS"));
				cust.setE_tax_id(rs.getString("TAX_ID"));
				System.out.println("Masuk");
				System.out.println(rs.getString("NAME"));
				custList.add(cust);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return custList;
	}

	public ArrayList<Customer> getCustomerById(String c_id) throws Exception {

		ArrayList<Customer> custList = new ArrayList<Customer>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_CUSTOMER_BY_ID(?,?)}");
			cs.setString(1, c_id);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				Customer cust = new Customer();
				cust.setId_number(rs.getString("ID_NUMBER"));
				cust.setName(rs.getString("NAME"));
				cust.setFullname(rs.getString("FULLNAME"));
				cust.setId_type(rs.getString("ID_TYPE"));
				cust.setId_exp_date(rs.getDate("ID_EXP_DATE").toString());
				cust.setGender(rs.getString("GENDER"));
				cust.setPob(rs.getString("PLACE_OF_BIRTH"));
				cust.setCob(rs.getString("CITY"));
				cust.setProb(rs.getString("PROVINCE"));
				cust.setDob(rs.getDate("DATE_OF_BIRTH").toString());
				cust.setM_status(rs.getString("MARITAL_STATUS"));
				cust.setNo_dep(rs.getInt("NO_DEPENDENT"));
				cust.setEdu_level(rs.getString("EDUCATION_LEVEL"));
				cust.setMother_name(rs.getString("MOTHER_NAME"));
				cust.setR_address_1(rs.getString("ADDRESS"));
				cust.setE_tax_id(rs.getString("TAX_ID"));
				custList.add(cust);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return custList;
	}
	
	public ArrayList<Application> getApplicationById(String c_id) throws Exception {

		ArrayList<Application> appList = new ArrayList<Application>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_APPLICATION_BY_ID(?,?)}");
			cs.setString(1, c_id);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				Application app = new Application();
				app.setRef_no(rs.getString("REF_NUMBER"));
				app.setDate_created(rs.getString("DATE_CREATED"));
				app.setCreator(rs.getString("CREATED_BY"));
				app.setStatus(rs.getString("STATUS"));
				app.setHold_by(rs.getString("HOLD_BY"));
				appList.add(app);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return appList;
	}

	public ArrayList<Customer> getCustomerByName(String name) throws Exception {

		ArrayList<Customer> custList = new ArrayList<Customer>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_CUSTOMER_BY_NAME(?,?)}");
			cs.setString(1, name);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				Customer cust = new Customer();
				cust.setId_number(rs.getString("ID_NUMBER"));
				cust.setName(rs.getString("NAME"));
				cust.setFullname(rs.getString("FULLNAME"));
				cust.setId_type(rs.getString("ID_TYPE"));
				cust.setId_exp_date(rs.getDate("ID_EXP_DATE").toString());
				cust.setGender(rs.getString("GENDER"));
				cust.setPob(rs.getString("PLACE_OF_BIRTH"));
				cust.setCob(rs.getString("CITY"));
				cust.setProb(rs.getString("PROVINCE"));
				cust.setDob(rs.getDate("DATE_OF_BIRTH").toString());
				cust.setM_status(rs.getString("MARITAL_STATUS"));
				cust.setNo_dep(rs.getInt("NO_DEPENDENT"));
				cust.setEdu_level(rs.getString("EDUCATION_LEVEL"));
				cust.setMother_name(rs.getString("MOTHER_NAME"));
				cust.setR_address_1(rs.getString("ADDRESS"));
				cust.setE_tax_id(rs.getString("TAX_ID"));
				custList.add(cust);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return custList;
	}

	public void addCustomer(
			String id_number, String name, String fullname, String id_type, String id_exp_date,
			String gender, String pob, String cob, String prob, String dob,
			String m_status, int no_dep, String edu_level, String mother_name, String e_company_name,
			String e_emp_type, String e_address_1, String e_address_2, String e_address_3, String e_address_4,
			String e_city, String e_province, String e_zip_code, String e_office_phone, String e_extension,
			String e_occupation, String e_designation, String e_job_sector, String e_tax_indicator, String e_tax_id,
			int e_monthly_income, int e_other_income, int e_c_loan, int e_c_low_year, int e_c_low_month,
			String e_p_company_name, int e_p_low_year, int e_p_low_month, String mobil, String mobil_kredit,
			String motor, String motor_kredit, String askes, String ditanggung_kantor, String asuransi_pribadi,
			String r_mailing_address, String r_address_1, String r_address_2, String r_address_3, String r_address_4,
			String r_city, String r_province, String r_zip_code, String r_kelurahan, String r_kecamatan,
			String r_home_phone, String r_mobile_phone, String r_email, String r_status, int r_los_year,
			int r_los_month, String s_name, String s_id_number, String s_dob, String s_p_agree
			) throws Exception {

		CallableStatement cs1 = null;
		CallableStatement cs2 = null;
		CallableStatement cs3 = null;
		CallableStatement cs4 = null;
		CallableStatement cs5 = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			
			cs1 = conn.prepareCall("CALL ADD_CUSTOMER(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			cs1.setString(1, id_number);
			cs1.setString(2, name);
			cs1.setString(3, fullname);
			cs1.setString(4, id_type);
			cs1.setString(5, id_exp_date);
			cs1.setString(6, gender);
			cs1.setString(7, pob);
			cs1.setString(8, cob);
			cs1.setString(9, prob);
			cs1.setString(10, dob);
			cs1.setString(11, m_status);
			cs1.setInt(12, no_dep);
			cs1.setString(13, edu_level);
			cs1.setString(14, mother_name);
			cs1.execute();
			cs1.close();
			
			cs2 = conn.prepareCall("CALL ADD_CUSTOMER_SPOUSE(?,?,?,?,?)");
			cs2.setString(1, id_number);
			cs2.setString(2, s_name);
			cs2.setString(3, s_id_number);
			cs2.setString(4, s_dob);
			cs2.setString(5, s_p_agree);
			cs2.execute();
			cs2.close();
			
			cs3 = conn.prepareCall("CALL ADD_CUSTOMER_RESIDENCE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			cs3.setString(1, id_number);
			cs3.setString(2, r_mailing_address);
			cs3.setString(3, r_address_1);
			cs3.setString(4, r_address_2);
			cs3.setString(5, r_address_3);
			cs3.setString(6, r_address_4);
			cs3.setString(7, r_city);
			cs3.setString(8, r_province);
			cs3.setString(9, r_zip_code);
			cs3.setString(10, r_kelurahan);
			cs3.setString(11, r_kecamatan);
			cs3.setString(12, r_home_phone);
			cs3.setString(13, r_mobile_phone);
			cs3.setString(14, r_email);
			cs3.setString(15, r_status);
			cs3.setInt(16, r_los_year);
			cs3.setInt(17, r_los_year);
			cs3.execute();
			cs3.close();
			
			cs4 = conn.prepareCall("CALL ADD_CUSTOMER_EMPLOYMENT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			cs4.setString(1, id_number);
			cs4.setString(2, e_company_name);
			cs4.setString(3, e_emp_type);
			cs4.setString(4, e_address_1);
			cs4.setString(5, e_address_2);
			cs4.setString(6, e_address_3);
			cs4.setString(7, e_address_4);
			cs4.setString(8, e_city);
			cs4.setString(9, e_province);
			cs4.setString(10, e_zip_code);
			cs4.setString(11, e_office_phone);
			cs4.setString(12, e_extension);
			cs4.setString(13, e_occupation);
			cs4.setString(14, e_designation);
			cs4.setString(15, e_job_sector);
			cs4.setString(16, e_tax_indicator);
			cs4.setString(17, e_tax_id);
			cs4.setInt(18, e_monthly_income);
			cs4.setInt(19, e_other_income);
			cs4.setInt(20, e_c_loan);
			cs4.setInt(21, e_c_low_year);
			cs4.setInt(22, e_c_low_month);
			cs4.setString(23, e_p_company_name);
			cs4.setInt(24, e_p_low_year);
			cs4.setInt(25, e_p_low_month);
			cs4.execute();
			cs4.close();
			
			cs5 = conn.prepareCall("CALL ADD_CUSTOMER_OTHERS(?,?,?,?,?,?,?,?)");
			cs5.setString(1, id_number);
			if(mobil.equals("Yes")){
				cs5.setInt(2, 1);
			}else{
				cs5.setInt(2, 2);
			}
			if(mobil_kredit.equals("Yes")){
				cs5.setInt(3, 1);
			}else{
				cs5.setInt(3, 2);
			}
			if(motor.equals("Yes")){
				cs5.setInt(4, 1);
			}else{
				cs5.setInt(4, 2);
			}
			if(motor_kredit.equals("Yes")){
				cs5.setInt(5, 1);
			}else{
				cs5.setInt(5, 2);
			}
			if(askes.equals("Yes")){
				cs5.setInt(6, 1);
			}else{
				cs5.setInt(6, 2);
			}
			if(ditanggung_kantor.equals("Yes")){
				cs5.setInt(7, 1);
			}else{
				cs5.setInt(7, 2);
			}
			if(asuransi_pribadi.equals("Yes")){
				cs5.setInt(8, 1);
			}else{
				cs5.setInt(8, 2);
			}
			cs5.execute();
			cs5.close();
			
			System.out.println("Data added");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new CCOSException(e.getErrorCode());
		}finally {
			conn.close();
		}
	}

	public Customer getCustomerDetail(String c_id) throws Exception {

		Customer cust = new Customer();
		CallableStatement cs1 = null;
		CallableStatement cs2 = null;
		CallableStatement cs3 = null;
		CallableStatement cs4 = null;
		CallableStatement cs5 = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			
			cs1 = conn.prepareCall("{CALL GET_CUSTOMER_DETAIL(?,?)}");
			cs1.setString(1, c_id);
			cs1.registerOutParameter(2, OracleTypes.CURSOR);
			cs1.execute();
			ResultSet rs1 = (ResultSet) cs1.getObject(2);
			while (rs1.next()) {
				cust.setId_number(rs1.getString("ID_NUMBER"));
				cust.setName(rs1.getString("NAME"));
				cust.setFullname(rs1.getString("FULLNAME"));
				cust.setId_type(rs1.getString("ID_TYPE"));
				cust.setId_exp_date(rs1.getString("ID_EXP_DATE"));
				cust.setGender(rs1.getString("GENDER"));
				cust.setPob(rs1.getString("PLACE_OF_BIRTH"));
				cust.setCob(rs1.getString("CITY"));
				cust.setProb(rs1.getString("PROVINCE"));
				cust.setDob(rs1.getString("DATE_OF_BIRTH"));
				cust.setM_status(rs1.getString("MARITAL_STATUS"));
				cust.setNo_dep(rs1.getInt("NO_DEPENDENT"));
				cust.setEdu_level(rs1.getString("EDUCATION_LEVEL"));
				cust.setMother_name(rs1.getString("MOTHER_NAME"));
			}
			
			cs2 = conn.prepareCall("{CALL GET_CUSTOMER_SPOUSE(?,?)}");
			cs2.setString(1, c_id);
			cs2.registerOutParameter(2, OracleTypes.CURSOR);
			cs2.execute();
			ResultSet rs2 = (ResultSet) cs2.getObject(2);
			while (rs2.next()) {
				cust.setS_name(rs2.getString("NAME"));
				cust.setS_id_number(rs2.getString("ID_NUMBER"));
				cust.setS_dob(rs2.getString("DATE_OF_BIRTH"));
				cust.setS_p_agree(rs2.getString("P_AGREEMENT"));
			}
			
			cs3 = conn.prepareCall("{CALL GET_CUSTOMER_RESIDENCE(?,?)}");
			cs3.setString(1, c_id);
			cs3.registerOutParameter(2, OracleTypes.CURSOR);
			cs3.execute();
			ResultSet rs3 = (ResultSet) cs3.getObject(2);
			while (rs3.next()) {
				cust.setR_mailing_address(rs3.getString("MAILING_ADDRESS"));
				cust.setR_address_1(rs3.getString("ADDRESS_1"));
				cust.setR_address_2(rs3.getString("ADDRESS_2"));
				cust.setR_address_3(rs3.getString("ADDRESS_3"));
				cust.setR_address_4(rs3.getString("ADDRESS_4"));
				cust.setR_city(rs3.getString("CITY"));
				cust.setR_province(rs3.getString("PROVINCE"));
				cust.setR_zip_code(rs3.getString("ZIP_CODE"));
				cust.setR_kelurahan(rs3.getString("KELURAHAN"));
				cust.setR_kecamatan(rs3.getString("KECAMATAN"));
				cust.setR_home_phone(rs3.getString("HOME_PHONE"));
				cust.setR_mobile_phone(rs3.getString("MOBILE_PHONE"));
				cust.setR_email(rs3.getString("EMAIL"));
				cust.setR_status(rs3.getString("STATUS"));
				cust.setR_los_year(rs3.getInt("LOS_YEAR"));
				cust.setR_los_month(rs3.getInt("LOS_MONTH"));
			}
			
			cs4 = conn.prepareCall("{CALL GET_CUSTOMER_EMPLOYMENT(?,?)}");
			cs4.setString(1, c_id);
			cs4.registerOutParameter(2, OracleTypes.CURSOR);
			cs4.execute();
			ResultSet rs4 = (ResultSet) cs4.getObject(2);
			while (rs4.next()) {
				cust.setE_company_name(rs4.getString("COMPANY_NAME"));
				cust.setE_emp_type(rs4.getString("EMP_TYPE"));
				cust.setE_address_1(rs4.getString("ADDRESS_1"));
				cust.setE_address_2(rs4.getString("ADDRESS_2"));
				cust.setE_address_3(rs4.getString("ADDRESS_3"));
				cust.setE_address_4(rs4.getString("ADDRESS_4"));
				cust.setE_city(rs4.getString("CITY"));
				cust.setE_province(rs4.getString("PROVINCE"));
				cust.setE_zip_code(rs4.getString("ZIP_CODE"));
				cust.setE_office_phone(rs4.getString("OFFICE_PHONE"));
				cust.setE_extension(rs4.getString("EXTENSION"));
				cust.setE_occupation(rs4.getString("OCCUPATION"));
				cust.setE_designation(rs4.getString("DESIGNATION"));
				cust.setE_job_sector(rs4.getString("JOB_SECTOR"));
				cust.setE_tax_indicator(rs4.getString("TAX_INDICATOR"));
				cust.setE_tax_id(rs4.getString("TAX_ID"));
				cust.setE_monthly_income(rs4.getInt("MONTHLY_INCOME"));
				cust.setE_other_income(rs4.getInt("OTHER_INCOME"));
				cust.setE_c_loan(rs4.getInt("C_LOAN"));
				cust.setE_c_low_year(rs4.getInt("C_LOW_YEAR"));
				cust.setE_c_low_month(rs4.getInt("C_LOW_MONTH"));
				cust.setE_p_company_name(rs4.getString("P_COMPANY_NAME"));
				cust.setE_p_low_year(rs4.getInt("P_LOW_YEAR"));
				cust.setE_p_low_month(rs4.getInt("P_LOW_MONTH"));
			}
			
			cs5 = conn.prepareCall("{CALL GET_CUSTOMER_OTHERS(?,?)}");
			cs5.setString(1, c_id);
			cs5.registerOutParameter(2, OracleTypes.CURSOR);
			cs5.execute();
			ResultSet rs5 = (ResultSet) cs5.getObject(2);
			while (rs5.next()) {
				if(rs5.getInt("MOBIL")==1){
					cust.setMobil("Yes");
				}else{
					cust.setMobil("No");
				}
				if(rs5.getInt("MOBIL_KREDIT")==1){
					cust.setMobil_kredit("Yes");
				}else{
					cust.setMobil_kredit("No");
				}
				if(rs5.getInt("MOTOR")==1){
					cust.setMotor("Yes");
				}else{
					cust.setMotor("No");
				}
				if(rs5.getInt("MOTOR_KREDIT")==1){
					cust.setMotor_kredit("Yes");
				}else{
					cust.setMotor_kredit("No");
				}
				if(rs5.getInt("ASKES")==1){
					cust.setAskes("Yes");
				}else{
					cust.setAskes("No");
				}
				if(rs5.getInt("DITANGGUNG_KANTOR")==1){
					cust.setDitanggung_kantor("Yes");
				}else{
					cust.setDitanggung_kantor("No");
				}
				if(rs5.getInt("ASURANSI_PRIBADI")==1){
					cust.setAsuransi_pribadi("Yes");
				}else{
					cust.setAsuransi_pribadi("No");
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs1.close();
			cs2.close();
			cs3.close();
			cs4.close();
			cs5.close();
			conn.close();
		}
		return cust;
	}

	
	public void addApplication(String u_id, String c_id, String a_ref_branch, String a_facility, String a_purpose, String a_business
			,String a_media,String a_fee,String a_provision,String a_kckk,String a_staff_name
			,String a_staf_nip,String a_staff_branch,String a_staff_account_no, String uname) throws Exception{
			
			CallableStatement cs = null;
			Statement stmt = null;
			Connection conn = null;
			Connection conn1 = null;
			LocalDate localDate = LocalDate.now();
			String year = localDate.format(DateTimeFormatter.ofPattern("yyyy"));
			String status = "Pending Data Entry";
			System.out.println(year);
			System.out.println(status);
			conn1 = ds.getConnection();
			stmt=conn1.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT REF_NUMBER FROM T_APPLICATION WHERE DATE_CREATED = ( SELECT MAX(DATE_CREATED) FROM T_APPLICATION )");
			String ref_num="";
			while(rs.next()){
				ref_num=rs.getString("REF_NUMBER");
			}
			System.out.println(ref_num);
			String [] splitted= ref_num.split("/");
			ref_num=splitted[2];
			System.out.println("Splitted"+ref_num);
			int inc = Integer.parseInt(ref_num)+1;
			System.out.println("increment"+inc);

			String value = String.format("%05d", inc);
			System.out.println("Changed to string"+value);
			
			
			String ref_number= "0960/500/"+value+"/"+year;
			System.out.println(ref_number);
			
			try {
				conn = ds.getConnection();
				
						
				cs = conn.prepareCall("CALL ADD_APPLICATION(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				cs.setString(1, ref_number);
				cs.setString(2, u_id);
				cs.setString(3, c_id);
				cs.setString(4, a_ref_branch);
				cs.setString(5, a_facility);
				cs.setString(6, a_purpose);
				cs.setString(7, a_business);
				cs.setString(8, a_media);
				cs.setString(9, a_fee);
				cs.setString(10, a_provision);
				cs.setString(11, a_kckk);
				cs.setString(12, a_staff_name);
				cs.setString(13, a_staf_nip);
				cs.setString(14, a_staff_branch);
				cs.setString(15, a_staff_account_no);
				cs.setString(16, uname);
				cs.setString(17, status);
				cs.setString(18, uname);
				cs.execute();
				System.out.println("Data added");
			} catch (SQLException e) {
				e.printStackTrace();
				throw new CCOSException(e.getErrorCode());
			}finally {
				cs.close();
				conn.close();
			}
		
	}

	public void updateCustomer(
			String id_number, String name, String fullname, String id_type, String id_exp_date,
			String gender, String pob, String cob, String prob, String dob,
			String m_status, int no_dep, String edu_level, String mother_name, String e_company_name,
			String e_emp_type, String e_address_1, String e_address_2, String e_address_3, String e_address_4,
			String e_city, String e_province, String e_zip_code, String e_office_phone, String e_extension,
			String e_occupation, String e_designation, String e_job_sector, String e_tax_indicator, String e_tax_id,
			int e_monthly_income, int e_other_income, int e_c_loan, int e_c_low_year, int e_c_low_month,
			String e_p_company_name, int e_p_low_year, int e_p_low_month, String mobil, String mobil_kredit,
			String motor, String motor_kredit, String askes, String ditanggung_kantor, String asuransi_pribadi,
			String r_mailing_address, String r_address_1, String r_address_2, String r_address_3, String r_address_4,
			String r_city, String r_province, String r_zip_code, String r_kelurahan, String r_kecamatan,
			String r_home_phone, String r_mobile_phone, String r_email, String r_status, int r_los_year,
			int r_los_month, String s_name, String s_id_number, String s_dob, String s_p_agree
			) throws Exception {

		CallableStatement cs1 = null;
		CallableStatement cs2 = null;
		CallableStatement cs3 = null;
		CallableStatement cs4 = null;
		CallableStatement cs5 = null;
		Connection conn1 = null;
		Connection conn2 = null;
		Connection conn3 = null;
		Connection conn4 = null;
		Connection conn5 = null;

		try {
			conn1 = ds.getConnection();
			conn2 = ds.getConnection();
			conn3 = ds.getConnection();
			conn4 = ds.getConnection();
			conn5 = ds.getConnection();
			
			cs1 = conn1.prepareCall("CALL UPDATE_CUSTOMER(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			cs1.setString(1, id_number);
			cs1.setString(2, name);
			cs1.setString(3, fullname);
			cs1.setString(4, id_type);
		    cs1.setString(5, id_exp_date);
			cs1.setString(6, gender);
			cs1.setString(7, pob);
			cs1.setString(8, cob);
			cs1.setString(9, prob);
			cs1.setString(10, dob);
			cs1.setString(11, m_status);
			cs1.setInt(12, no_dep);
			cs1.setString(13, edu_level);
			cs1.setString(14, mother_name);
			cs1.execute();
			cs1.close();
			
			cs2 = conn2.prepareCall("CALL UPDATE_CUSTOMER_SPOUSE(?,?,?,?,?)");
			cs2.setString(1, id_number);
			cs2.setString(2, s_name);
			cs2.setString(3, s_id_number);
			cs2.setString(4, s_dob);
			cs2.setString(5, s_p_agree);
			cs2.execute();
			cs2.close();
			
			cs3 = conn3.prepareCall("CALL UPDATE_CUSTOMER_RESIDENCE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			cs3.setString(1, id_number);
			cs3.setString(2, r_mailing_address);
			cs3.setString(3, r_address_1);
			cs3.setString(4, r_address_2);
			cs3.setString(5, r_address_3);
			cs3.setString(6, r_address_4);
			cs3.setString(7, r_city);
			cs3.setString(8, r_province);
			cs3.setString(9, r_zip_code);
			cs3.setString(10, r_kelurahan);
			cs3.setString(11, r_kecamatan);
			cs3.setString(12, r_home_phone);
			cs3.setString(13, r_mobile_phone);
			cs3.setString(14, r_email);
			cs3.setString(15, r_status);
			cs3.setInt(16, r_los_year);
			cs3.setInt(17, r_los_year);
			cs3.execute();
			
			cs4 = conn4.prepareCall("CALL UPDATE_CUSTOMER_EMPLOYMENT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			cs4.setString(1, id_number);
			cs4.setString(2, e_company_name);
			cs4.setString(3, e_emp_type);
			cs4.setString(4, e_address_1);
			cs4.setString(5, e_address_2);
			cs4.setString(6, e_address_3);
			cs4.setString(7, e_address_4);
			cs4.setString(8, e_city);
			cs4.setString(9, e_province);
			cs4.setString(10, e_zip_code);
			cs4.setString(11, e_office_phone);
			cs4.setString(12, e_extension);
			cs4.setString(13, e_occupation);
			cs4.setString(14, e_designation);
			cs4.setString(15, e_job_sector);
			cs4.setString(16, e_tax_indicator);
			cs4.setString(17, e_tax_id);
			cs4.setInt(18, e_monthly_income);
			cs4.setInt(19, e_other_income);
			cs4.setInt(20, e_c_loan);
			cs4.setInt(21, e_c_low_year);
			cs4.setInt(22, e_c_low_month);
			cs4.setString(23, e_p_company_name);
			cs4.setInt(24, e_p_low_year);
			cs4.setInt(25, e_p_low_month);
			cs4.execute();
			cs4.close();
			
			cs5 = conn5.prepareCall("CALL UPDATE_CUSTOMER_OTHERS(?,?,?,?,?,?,?,?)");
			cs5.setString(1, id_number);
			if(mobil.equals("Yes")){
				cs5.setInt(2, 1);
			}else{
				cs5.setInt(2, 2);
			}
			if(mobil_kredit.equals("Yes")){
				cs5.setInt(3, 1);
			}else{
				cs5.setInt(3, 2);
			}
			if(motor.equals("Yes")){
				cs5.setInt(4, 1);
			}else{
				cs5.setInt(4, 2);
			}
			if(motor_kredit.equals("Yes")){
				cs5.setInt(5, 1);
			}else{
				cs5.setInt(5, 2);
			}
			if(askes.equals("Yes")){
				cs5.setInt(6, 1);
			}else{
				cs5.setInt(6, 2);
			}
			if(ditanggung_kantor.equals("Yes")){
				cs5.setInt(7, 1);
			}else{
				cs5.setInt(7, 2);
			}
			if(asuransi_pribadi.equals("Yes")){
				cs5.setInt(8, 1);
			}else{
				cs5.setInt(8, 2);
			}
			cs5.execute();

			cs5.close();
			
			System.out.println("Data updated");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn1.close();
			conn2.close();
			conn3.close();
			conn4.close();
			conn5.close();
		}
	}

	public void deleteCustomer(String c_id) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("CALL DELETE_CUSTOMER(?)");
			cs.setString(1, c_id);
			cs.execute();
			System.out.println("Data deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
	}
	
	public ArrayList<City> getAllCity() throws Exception {

		ArrayList<City> cityList = new ArrayList<City>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_CITY(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				City city = new City();
				city.setCode(rs.getString("CODE"));
				city.setDescription(rs.getString("DESCRIPTION"));
				city.setProvince(rs.getString("PROVINCE"));
				city.setDate_created(rs.getDate("DATE_CREATED"));
				city.setCreated_by(rs.getString("CREATED_BY"));
				city.setDate_updated(rs.getDate("DATE_UPDATED"));
				city.setUpdated_by(rs.getString("UPDATED_BY"));
				cityList.add(city);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return cityList;
	}
	
	public ArrayList<Province> getAllProvince() throws Exception {

		ArrayList<Province> provList = new ArrayList<Province>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_PROVINCE(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				Province prov = new Province();
				prov.setCode(rs.getString("CODE"));
				prov.setDescription(rs.getString("DESCRIPTION"));
				prov.setDate_created(rs.getDate("DATE_CREATED"));
				prov.setCreated_by(rs.getString("CREATED_BY"));
				prov.setDate_updated(rs.getDate("DATE_UPDATED"));
				prov.setUpdated_by(rs.getString("UPDATED_BY"));

				provList.add(prov);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return provList;
	}
	
	public ArrayList<Occupation> getAllOccupation() throws Exception {

		ArrayList<Occupation> occList = new ArrayList<Occupation>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_OCCUPATION(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				Occupation occ = new Occupation();
				occ.setCode(rs.getString("CODE"));
				occ.setDescription(rs.getString("DESCRIPTION"));
				if(rs.getInt("IS_RISKY")==1){
					occ.setIs_risky("Yes");
				}else{
					occ.setIs_risky("No");
				}
				occ.setIls_code(rs.getString("ILS_CODE"));
				occ.setScoreware_code(rs.getString("SCOREWARE_CODE"));
				if(rs.getInt("IS_DEACTIVATED")==1){
					occ.setIs_deactivated("Yes");
				}else{
					occ.setIs_deactivated("No");
				}
				occ.setDate_created(rs.getDate("DATE_CREATED"));
				occ.setCreated_by(rs.getString("CREATED_BY"));
				occ.setDate_updated(rs.getDate("DATE_UPDATED"));
				occ.setUpdated_by(rs.getString("UPDATED_BY"));

				occList.add(occ);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return occList;
	}
}
