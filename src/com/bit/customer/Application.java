package com.bit.customer;

public class Application {
	private String ref_no;
	private String date_created;
	private String creator;
	private String status;
	private String hold_by;
	public String getRef_no() {
		return ref_no;
	}
	public void setRef_no(String ref_no) {
		this.ref_no = ref_no;
	}
	public String getDate_created() {
		return date_created;
	}
	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHold_by() {
		return hold_by;
	}
	public void setHold_by(String hold_by) {
		this.hold_by = hold_by;
	}
	
	
}
