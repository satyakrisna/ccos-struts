package com.bit.customer;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.bit.city.City;
import com.bit.occupation.Occupation;
import com.bit.province.Province;

public class CustomerForm extends ActionForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String task;
	private String search="id";
	private String value;
	private ArrayList<Customer> custList;
	private ArrayList<Province> provList;
	private ArrayList<City> cityList;
	private ArrayList<Occupation> occList;
	private ArrayList<Application> appList;
	
	private String id_number;
	private String name;
	private String fullname;
	private String id_type;
	private String id_exp_date;
	private String gender;
	private String pob;
	private String cob;
	private String prob;
	private String dob;
	
	private String m_status;
	private int no_dep;
	private String edu_level;
	private String mother_name;
	private String e_company_name;
	private String e_emp_type;
	private String e_address_1;
	private String e_address_2;
	private String e_address_3;
	private String e_address_4;
	
	private String e_city;
	private String e_province;
	private String e_zip_code;
	private String e_office_phone;
	private String e_extension;
	private String e_occupation;
	private String e_designation;
	private String e_job_sector;
	private String e_tax_indicator;
	private String e_tax_id;
	
	private int e_monthly_income;
	private int e_other_income;
	private int e_c_loan;
	private int e_c_low_year;
	private int e_c_low_month;
	private String e_p_company_name;
	private int e_p_low_year;
	private int e_p_low_month;
	private String mobil;
	private String mobil_kredit;
	
	private String motor;
	private String motor_kredit;
	private String askes;
	private String ditanggung_kantor;
	private String asuransi_pribadi;
	private String r_mailing_address;
	private String r_address_1;
	private String r_address_2;
	private String r_address_3;
	private String r_address_4;
	
	private String r_city;
	private String r_province;
	private String r_zip_code;
	private String r_kelurahan;
	private String r_kecamatan;
	private String r_home_phone;
	private String r_mobile_phone;
	private String r_email;
	private String r_status;
	private int r_los_year;
	
	private int r_los_month;
	private String s_name;
	private String s_id_number;
	private String s_dob;
	private String s_p_agree;
	
	private String a_ref_branch;
	private String a_date_received;
	private String a_facility;
	private String a_purpose;
	private String a_business;
	private String a_media;
	private String a_fee;
	private String a_provision;
	private String a_kckk;
	private String a_staff_name;
	
	private String a_staf_nip;
	private String a_staff_branch;
	private String a_staff_account_no;
	
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public ArrayList<Customer> getCustList() {
		return custList;
	}
	public void setCustList(ArrayList<Customer> custList) {
		this.custList = custList;
	}
	public ArrayList<Province> getProvList() {
		return provList;
	}
	public void setProvList(ArrayList<Province> provList) {
		this.provList = provList;
	}
	public ArrayList<City> getCityList() {
		return cityList;
	}
	public void setCityList(ArrayList<City> cityList) {
		this.cityList = cityList;
	}
	public ArrayList<Occupation> getOccList() {
		return occList;
	}
	public void setOccList(ArrayList<Occupation> occList) {
		this.occList = occList;
	}
	public String getId_number() {
		return id_number;
	}
	public void setId_number(String id_number) {
		this.id_number = id_number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getId_type() {
		return id_type;
	}
	public void setId_type(String id_type) {
		this.id_type = id_type;
	}
	public String getId_exp_date() {
		return id_exp_date;
	}
	public void setId_exp_date(String id_exp_date) {
		this.id_exp_date = id_exp_date;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPob() {
		return pob;
	}
	public void setPob(String pob) {
		this.pob = pob;
	}
	public String getCob() {
		return cob;
	}
	public void setCob(String cob) {
		this.cob = cob;
	}
	public String getProb() {
		return prob;
	}
	public void setProb(String prob) {
		this.prob = prob;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getM_status() {
		return m_status;
	}
	public void setM_status(String m_status) {
		this.m_status = m_status;
	}
	public int getNo_dep() {
		return no_dep;
	}
	public void setNo_dep(int no_dep) {
		this.no_dep = no_dep;
	}
	public String getEdu_level() {
		return edu_level;
	}
	public void setEdu_level(String edu_level) {
		this.edu_level = edu_level;
	}
	public String getMother_name() {
		return mother_name;
	}
	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}
	public String getE_company_name() {
		return e_company_name;
	}
	public void setE_company_name(String e_company_name) {
		this.e_company_name = e_company_name;
	}
	public String getE_emp_type() {
		return e_emp_type;
	}
	public void setE_emp_type(String e_emp_type) {
		this.e_emp_type = e_emp_type;
	}
	public String getE_address_1() {
		return e_address_1;
	}
	public void setE_address_1(String e_address_1) {
		this.e_address_1 = e_address_1;
	}
	public String getE_address_2() {
		return e_address_2;
	}
	public void setE_address_2(String e_address_2) {
		this.e_address_2 = e_address_2;
	}
	public String getE_address_3() {
		return e_address_3;
	}
	public void setE_address_3(String e_address_3) {
		this.e_address_3 = e_address_3;
	}
	public String getE_address_4() {
		return e_address_4;
	}
	public void setE_address_4(String e_address_4) {
		this.e_address_4 = e_address_4;
	}
	public String getE_city() {
		return e_city;
	}
	public void setE_city(String e_city) {
		this.e_city = e_city;
	}
	public String getE_province() {
		return e_province;
	}
	public void setE_province(String e_province) {
		this.e_province = e_province;
	}
	public String getE_zip_code() {
		return e_zip_code;
	}
	public void setE_zip_code(String e_zip_code) {
		this.e_zip_code = e_zip_code;
	}
	public String getE_office_phone() {
		return e_office_phone;
	}
	public void setE_office_phone(String e_office_phone) {
		this.e_office_phone = e_office_phone;
	}
	public String getE_extension() {
		return e_extension;
	}
	public void setE_extension(String e_extension) {
		this.e_extension = e_extension;
	}
	public String getE_occupation() {
		return e_occupation;
	}
	public void setE_occupation(String e_occupation) {
		this.e_occupation = e_occupation;
	}
	public String getE_designation() {
		return e_designation;
	}
	public void setE_designation(String e_designation) {
		this.e_designation = e_designation;
	}
	public String getE_job_sector() {
		return e_job_sector;
	}
	public void setE_job_sector(String e_job_sector) {
		this.e_job_sector = e_job_sector;
	}
	public String getE_tax_indicator() {
		return e_tax_indicator;
	}
	public void setE_tax_indicator(String e_tax_indicator) {
		this.e_tax_indicator = e_tax_indicator;
	}
	public String getE_tax_id() {
		return e_tax_id;
	}
	public void setE_tax_id(String e_tax_id) {
		this.e_tax_id = e_tax_id;
	}
	public int getE_monthly_income() {
		return e_monthly_income;
	}
	public void setE_monthly_income(int e_monthly_income) {
		this.e_monthly_income = e_monthly_income;
	}
	public int getE_other_income() {
		return e_other_income;
	}
	public void setE_other_income(int e_other_income) {
		this.e_other_income = e_other_income;
	}
	public int getE_c_loan() {
		return e_c_loan;
	}
	public void setE_c_loan(int e_c_loan) {
		this.e_c_loan = e_c_loan;
	}
	public int getE_c_low_year() {
		return e_c_low_year;
	}
	public void setE_c_low_year(int e_c_low_year) {
		this.e_c_low_year = e_c_low_year;
	}
	public int getE_c_low_month() {
		return e_c_low_month;
	}
	public void setE_c_low_month(int e_c_low_month) {
		this.e_c_low_month = e_c_low_month;
	}
	public String getE_p_company_name() {
		return e_p_company_name;
	}
	public void setE_p_company_name(String e_p_company_name) {
		this.e_p_company_name = e_p_company_name;
	}
	public int getE_p_low_year() {
		return e_p_low_year;
	}
	public void setE_p_low_year(int e_p_low_year) {
		this.e_p_low_year = e_p_low_year;
	}
	public int getE_p_low_month() {
		return e_p_low_month;
	}
	public void setE_p_low_month(int e_p_low_month) {
		this.e_p_low_month = e_p_low_month;
	}
	public String getMobil() {
		return mobil;
	}
	public void setMobil(String mobil) {
		this.mobil = mobil;
	}
	public String getMobil_kredit() {
		return mobil_kredit;
	}
	public void setMobil_kredit(String mobil_kredit) {
		this.mobil_kredit = mobil_kredit;
	}
	public String getMotor() {
		return motor;
	}
	public void setMotor(String motor) {
		this.motor = motor;
	}
	public String getMotor_kredit() {
		return motor_kredit;
	}
	public void setMotor_kredit(String motor_kredit) {
		this.motor_kredit = motor_kredit;
	}
	public String getAskes() {
		return askes;
	}
	public void setAskes(String askes) {
		this.askes = askes;
	}
	public String getDitanggung_kantor() {
		return ditanggung_kantor;
	}
	public void setDitanggung_kantor(String ditanggung_kantor) {
		this.ditanggung_kantor = ditanggung_kantor;
	}
	public String getAsuransi_pribadi() {
		return asuransi_pribadi;
	}
	public void setAsuransi_pribadi(String asuransi_pribadi) {
		this.asuransi_pribadi = asuransi_pribadi;
	}
	public String getR_mailing_address() {
		return r_mailing_address;
	}
	public void setR_mailing_address(String r_mailing_address) {
		this.r_mailing_address = r_mailing_address;
	}
	public String getR_address_1() {
		return r_address_1;
	}
	public void setR_address_1(String r_address_1) {
		this.r_address_1 = r_address_1;
	}
	public String getR_address_2() {
		return r_address_2;
	}
	public void setR_address_2(String r_address_2) {
		this.r_address_2 = r_address_2;
	}
	public String getR_address_3() {
		return r_address_3;
	}
	public void setR_address_3(String r_address_3) {
		this.r_address_3 = r_address_3;
	}
	public String getR_address_4() {
		return r_address_4;
	}
	public void setR_address_4(String r_address_4) {
		this.r_address_4 = r_address_4;
	}
	public String getR_city() {
		return r_city;
	}
	public void setR_city(String r_city) {
		this.r_city = r_city;
	}
	public String getR_province() {
		return r_province;
	}
	public void setR_province(String r_province) {
		this.r_province = r_province;
	}
	public String getR_zip_code() {
		return r_zip_code;
	}
	public void setR_zip_code(String r_zip_code) {
		this.r_zip_code = r_zip_code;
	}
	public String getR_kelurahan() {
		return r_kelurahan;
	}
	public void setR_kelurahan(String r_kelurahan) {
		this.r_kelurahan = r_kelurahan;
	}
	public String getR_kecamatan() {
		return r_kecamatan;
	}
	public void setR_kecamatan(String r_kecamatan) {
		this.r_kecamatan = r_kecamatan;
	}
	public String getR_home_phone() {
		return r_home_phone;
	}
	public void setR_home_phone(String r_home_phone) {
		this.r_home_phone = r_home_phone;
	}
	public String getR_mobile_phone() {
		return r_mobile_phone;
	}
	public void setR_mobile_phone(String r_mobile_phone) {
		this.r_mobile_phone = r_mobile_phone;
	}
	public String getR_email() {
		return r_email;
	}
	public void setR_email(String r_email) {
		this.r_email = r_email;
	}
	public String getR_status() {
		return r_status;
	}
	public void setR_status(String r_status) {
		this.r_status = r_status;
	}
	public int getR_los_year() {
		return r_los_year;
	}
	public void setR_los_year(int r_los_year) {
		this.r_los_year = r_los_year;
	}
	public int getR_los_month() {
		return r_los_month;
	}
	public void setR_los_month(int r_los_month) {
		this.r_los_month = r_los_month;
	}
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}
	public String getS_id_number() {
		return s_id_number;
	}
	public void setS_id_number(String s_id_number) {
		this.s_id_number = s_id_number;
	}
	public String getS_dob() {
		return s_dob;
	}
	public void setS_dob(String s_dob) {
		this.s_dob = s_dob;
	}
	public String getS_p_agree() {
		return s_p_agree;
	}
	public void setS_p_agree(String s_p_agree) {
		this.s_p_agree = s_p_agree;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public ArrayList<Application> getAppList() {
		return appList;
	}
	public void setAppList(ArrayList<Application> appList) {
		this.appList = appList;
	}
	public String getA_ref_branch() {
		return a_ref_branch;
	}
	public void setA_ref_branch(String a_ref_branch) {
		this.a_ref_branch = a_ref_branch;
	}
	public String getA_date_received() {
		return a_date_received;
	}
	public void setA_date_received(String a_date_received) {
		this.a_date_received = a_date_received;
	}
	public String getA_facility() {
		return a_facility;
	}
	public void setA_facility(String a_facility) {
		this.a_facility = a_facility;
	}
	public String getA_purpose() {
		return a_purpose;
	}
	public void setA_purpose(String a_purpose) {
		this.a_purpose = a_purpose;
	}
	public String getA_business() {
		return a_business;
	}
	public void setA_business(String a_business) {
		this.a_business = a_business;
	}
	public String getA_media() {
		return a_media;
	}
	public void setA_media(String a_media) {
		this.a_media = a_media;
	}
	public String getA_fee() {
		return a_fee;
	}
	public void setA_fee(String a_fee) {
		this.a_fee = a_fee;
	}
	public String getA_provision() {
		return a_provision;
	}
	public void setA_provision(String a_provision) {
		this.a_provision = a_provision;
	}
	public String getA_kckk() {
		return a_kckk;
	}
	public void setA_kckk(String a_kckk) {
		this.a_kckk = a_kckk;
	}
	public String getA_staff_name() {
		return a_staff_name;
	}
	public void setA_staff_name(String a_staff_name) {
		this.a_staff_name = a_staff_name;
	}
	public String getA_staf_nip() {
		return a_staf_nip;
	}
	public void setA_staf_nip(String a_staf_nip) {
		this.a_staf_nip = a_staf_nip;
	}
	public String getA_staff_branch() {
		return a_staff_branch;
	}
	public void setA_staff_branch(String a_staff_branch) {
		this.a_staff_branch = a_staff_branch;
	}
	public String getA_staff_account_no() {
		return a_staff_account_no;
	}
	public void setA_staff_account_no(String a_staff_account_no) {
		this.a_staff_account_no = a_staff_account_no;
	}
	
	

	
}
