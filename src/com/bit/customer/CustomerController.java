package com.bit.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import CCOS.CCOSException;

public class CustomerController extends Action{
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if(request.getSession().getAttribute("is_login")!=null){
			CustomerForm cf = (CustomerForm) form;
			CustomerManager cm = new CustomerManager();
			Customer c = new Customer();
			HttpSession session=request.getSession();
			
			String task = cf.getTask();
			String search = cf.getSearch();
			String value = cf.getValue();
			String username = (String)session.getAttribute("username");
			String user_id = (String)session.getAttribute("user_id");
			
			String a_ref_branch = cf.getA_ref_branch();
			String a_facility = cf.getA_facility();
			String a_purpose = cf.getA_purpose();
			String a_business = cf.getA_business();
			String a_media = cf.getA_media();
			String a_fee = cf.getA_fee();
			String a_provision = cf.getA_provision();
			String a_kckk = cf.getA_kckk();
			String a_staff_name = cf.getA_staff_name();
			String a_staf_nip = cf.getA_staf_nip();
			String a_staff_branch = cf.getA_staff_branch();
			String a_staff_account_no = cf.getA_staff_account_no();
			
			String id_number = cf.getId_number();
			String name = cf.getName();
			String fullname = cf.getFullname();
			String id_type = cf.getId_type();
			String id_exp_date = cf.getId_exp_date();
			String gender = cf.getGender();
			String pob = cf.getPob();
			String cob = cf.getCob();
			String prob = cf.getProb();
			String dob = cf.getDob();
			
			String m_status = cf.getM_status();
			int no_dep = cf.getNo_dep();
			String edu_level = cf.getEdu_level();
			String mother_name = cf.getMother_name();
			String e_company_name = cf.getE_company_name();
			String e_emp_type = cf.getE_emp_type();
			String e_address_1 = cf.getE_address_1();
			String e_address_2 = cf.getE_address_2();
			String e_address_3 = cf.getE_address_3();
			String e_address_4 = cf.getE_address_4();
			
			String e_city = cf.getE_city();
			String e_province = cf.getE_province();
			String e_zip_code = cf.getE_zip_code();
			String e_office_phone = cf.getE_office_phone();
			String e_extension = cf.getE_extension();
			String e_occupation = cf.getE_occupation();
			String e_designation = cf.getE_designation();
			String e_job_sector = cf.getE_job_sector();
			String e_tax_indicator = cf.getE_tax_indicator();
			String e_tax_id = cf.getE_tax_id();
			
			int e_monthly_income = cf.getE_monthly_income();
			int e_other_income = cf.getE_other_income();
			int e_c_loan = cf.getE_c_loan();
			int e_c_low_year = cf.getE_c_low_year();
			int e_c_low_month = cf.getE_c_low_month();
			String e_p_company_name = cf.getE_p_company_name();
			int e_p_low_year = cf.getE_p_low_year();
			int e_p_low_month = cf.getE_p_low_month();
			String mobil = cf.getMobil();
			String mobil_kredit = cf.getMobil_kredit();
			
			String motor = cf.getMotor();
			String motor_kredit = cf.getMobil_kredit();
			String askes = cf.getAskes();
			String ditanggung_kantor = cf.getDitanggung_kantor();
			String asuransi_pribadi = cf.getAsuransi_pribadi();
			String r_mailing_address = cf.getR_mailing_address();
			String r_address_1 = cf.getR_address_1();
			String r_address_2 = cf.getR_address_2();
			String r_address_3 = cf.getR_address_3();
			String r_address_4 = cf.getR_address_4();
			
			String r_city = cf.getR_city();
			String r_province = cf.getR_province();
			String r_zip_code = cf.getR_zip_code();
			String r_kelurahan = cf.getR_kelurahan();
			String r_kecamatan = cf.getR_kecamatan();
			String r_home_phone = cf.getR_home_phone();
			String r_mobile_phone = cf.getR_mobile_phone();
			String r_email = cf.getR_email();
			String r_status = cf.getR_status();
			int r_los_year = cf.getR_los_year();
			
			int r_los_month = cf.getR_los_month();
			String s_name = cf.getS_name();
			String s_id_number = cf.getS_id_number();
			String s_dob = cf.getS_dob();
			String s_p_agree = cf.getS_p_agree();
			
			String map = "";
			
			if (task == null||task.equals("load")) {
				System.out.println("aaaaa");
				cf.setCustList(cm.getAllCustomer());
				map = "list";
			} else if (task.equals("search") && search.equals("id")) {
				cf.setCustList(cm.getCustomerById(value));
				map = "search";
			} else if (task.equals("search") && search.equals("name")) {
				cf.setCustList(cm.getCustomerByName(value));
				map = "search";
			} else if (task.equals("add")) {
				resetCustomerForm(cf);
				cf.setProvList(cm.getAllProvince());
				cf.setCityList(cm.getAllCity());
				cf.setOccList(cm.getAllOccupation());
				map = "add";
			} else if (task.equals("save")) {
				try{
					cm.addCustomer(
							id_number, name, fullname, id_type, id_exp_date,
							gender, pob, cob, prob, dob,
							m_status, no_dep, edu_level, mother_name, e_company_name,
							e_emp_type, e_address_1, e_address_2, e_address_3, e_address_4,
							e_city, e_province, e_zip_code, e_office_phone, e_extension,
							e_occupation, e_designation, e_job_sector, e_tax_indicator, e_tax_id,
							e_monthly_income, e_other_income, e_c_loan, e_c_low_year, e_c_low_month,
							e_p_company_name, e_p_low_year, e_p_low_month, mobil, mobil_kredit,
							motor, motor_kredit, askes, ditanggung_kantor, asuransi_pribadi,
							r_mailing_address, r_address_1, r_address_2, r_address_3, r_address_4,
							r_city, r_province, r_zip_code, r_kelurahan, r_kecamatan,
							r_home_phone, r_mobile_phone, r_email, r_status, r_los_year,
							r_los_month, s_name, s_id_number, s_dob, s_p_agree
							);
					request.setAttribute("add_msg", "Data added.");
					c=cm.getCustomerDetail(id_number);
					cf.setAppList(cm.getApplicationById(id_number));
					setCustomerForm(cf, c);
					map = "detail";
				}catch(CCOSException e){
					request.setAttribute("error", "ERR["+e.getErrorCode()+"] "+e.getErrorMsg());
					map = "add";
				}
			} else if (task.equals("detail")) {
				c=cm.getCustomerDetail(id_number);
				cf.setAppList(cm.getApplicationById(id_number));
				setCustomerForm(cf, c);
				map = "detail";
			} else if (task.equals("edit")) {
				c=cm.getCustomerDetail(id_number);
				setCustomerForm(cf, c);
				cf.setProvList(cm.getAllProvince());
				cf.setCityList(cm.getAllCity());
				cf.setOccList(cm.getAllOccupation());
				map = "edit";
			} else if (task.equals("update")) {
				cm.updateCustomer(
						id_number, name, fullname, id_type, id_exp_date,
						gender, pob, cob, prob, dob,
						m_status, no_dep, edu_level, mother_name, e_company_name,
						e_emp_type, e_address_1, e_address_2, e_address_3, e_address_4,
						e_city, e_province, e_zip_code, e_office_phone, e_extension,
						e_occupation, e_designation, e_job_sector, e_tax_indicator, e_tax_id,
						e_monthly_income, e_other_income, e_c_loan, e_c_low_year, e_c_low_month,
						e_p_company_name, e_p_low_year, e_p_low_month, mobil, mobil_kredit,
						motor, motor_kredit, askes, ditanggung_kantor, asuransi_pribadi,
						r_mailing_address, r_address_1, r_address_2, r_address_3, r_address_4,
						r_city, r_province, r_zip_code, r_kelurahan, r_kecamatan,
						r_home_phone, r_mobile_phone, r_email, r_status, r_los_year,
						r_los_month, s_name, s_id_number, s_dob, s_p_agree
						);
				request.setAttribute("update_msg", "Data updated.");
				c=cm.getCustomerDetail(id_number);
				cf.setAppList(cm.getApplicationById(id_number));
				setCustomerForm(cf, c);
				map = "detail";
			} else if (task.equals("delete")) {
				cm.deleteCustomer(id_number);
				request.setAttribute("delete_msg", "Data deleted.");
				map = "list";
			} else if (task.equals("app")) {
				resetAppForm(cf);
				map="app";
			} else if (task.equals("saveapp")) {
				cm.addApplication(user_id, id_number, a_ref_branch, a_facility, a_purpose, a_business, a_media, a_fee, a_provision, a_kckk, a_staff_name, a_staf_nip, a_staff_branch, a_staff_account_no, username);
				c=cm.getCustomerDetail(id_number);
				cf.setAppList(cm.getApplicationById(id_number));
				setCustomerForm(cf, c);
				map = "detail";
			} else if (task.equals("logout")) {
				session.invalidate();
				map="login";
			}
			
			if(map.equals("list")){
				resetCustomerForm(cf);
				cf.setCustList(cm.getAllCustomer());
			}
			return mapping.findForward(map);
		}else{
			return mapping.findForward("login");
		}
	}

	public CustomerForm setCustomerForm(CustomerForm cf, Customer c) {
		cf.setId_number(c.getId_number());
		cf.setName(c.getName());
		cf.setFullname(c.getFullname());
		cf.setId_type(c.getId_type());
		cf.setId_exp_date(c.getId_exp_date());
		cf.setGender(c.getGender());
		cf.setPob(c.getPob());
		cf.setCob(c.getCob());
		cf.setProb(c.getProb());
		cf.setDob(c.getDob());
		
		cf.setM_status(c.getM_status());
		cf.setNo_dep(c.getNo_dep());
		cf.setEdu_level(c.getEdu_level());
		cf.setMother_name(c.getMother_name());
		cf.setE_company_name(c.getE_company_name());
		cf.setE_emp_type(c.getE_emp_type());
		cf.setE_address_1(c.getE_address_1());
		cf.setE_address_2(c.getE_address_2());
		cf.setE_address_3(c.getE_address_3());
		
		cf.setE_address_4(c.getE_address_4());
		cf.setE_city(c.getE_city());
		cf.setE_province(c.getE_province());
		cf.setE_zip_code(c.getE_zip_code());
		cf.setE_office_phone(c.getE_office_phone());
		cf.setE_extension(c.getE_extension());
		cf.setE_occupation(c.getE_occupation());
		cf.setE_designation(c.getE_designation());
		cf.setE_job_sector(c.getE_job_sector());
		cf.setE_tax_indicator(c.getE_tax_indicator());
		
		cf.setE_tax_id(c.getE_tax_id());
		cf.setE_monthly_income(c.getE_monthly_income());
		cf.setE_other_income(c.getE_other_income());
		cf.setE_c_loan(c.getE_c_loan());
		cf.setE_c_low_year(c.getE_c_low_year());
		cf.setE_c_low_month(c.getE_c_low_month());
		cf.setE_p_company_name(c.getE_p_company_name());
		cf.setE_p_low_year(c.getE_p_low_year());
		cf.setE_p_low_month(c.getE_p_low_month());
		cf.setMobil(c.getMobil());
		cf.setMobil_kredit(c.getMobil_kredit());
		
		cf.setMotor(c.getMotor());
		cf.setMotor_kredit(c.getMobil_kredit());
		cf.setAskes(c.getAskes());
		cf.setDitanggung_kantor(c.getDitanggung_kantor());
		cf.setAsuransi_pribadi(c.getAsuransi_pribadi());
		cf.setR_mailing_address(c.getR_mailing_address());
		cf.setR_address_1(c.getR_address_1());
		cf.setR_address_2(c.getR_address_2());
		cf.setR_address_3(c.getR_address_3());
		cf.setR_address_4(c.getR_address_4());
		
		cf.setR_city(c.getR_city());
		cf.setR_province(c.getR_province());
		cf.setR_zip_code(c.getR_zip_code());
		cf.setR_kelurahan(c.getR_kelurahan());
		cf.setR_kecamatan(c.getR_kecamatan());
		cf.setR_home_phone(c.getR_home_phone());
		cf.setR_mobile_phone(c.getR_mobile_phone());
		cf.setR_email(c.getR_email());
		cf.setR_status(c.getR_status());
		cf.setR_los_year(c.getR_los_year());
		
		cf.setR_los_month(c.getR_los_month());
		cf.setS_name(c.getS_name());
		cf.setS_id_number(c.getS_id_number());
		cf.setS_dob(c.getS_dob());
		cf.setS_p_agree(c.getS_p_agree());
		
		return cf;
	}

	public CustomerForm resetCustomerForm(CustomerForm cf) {
		cf.setSearch("id");
		cf.setValue("");
		
		cf.setId_number("");
		cf.setName("");
		cf.setFullname("");
		cf.setId_type("");
		cf.setId_exp_date("");
		cf.setGender("");
		cf.setPob("");
		cf.setCob("");
		cf.setProb("");
		cf.setDob("");
		
		cf.setM_status("");
		cf.setNo_dep(0);
		cf.setEdu_level("");
		cf.setMother_name("");
		cf.setE_company_name("");
		cf.setE_emp_type("");
		cf.setE_address_1("");
		cf.setE_address_2("");
		cf.setE_address_3("");
		
		cf.setE_address_4("");
		cf.setE_city("");
		cf.setE_province("");
		cf.setE_zip_code("");
		cf.setE_office_phone("");
		cf.setE_extension("");
		cf.setE_occupation("");
		cf.setE_designation("");
		cf.setE_job_sector("");
		cf.setE_tax_indicator("");
		
		cf.setE_tax_id("");
		cf.setE_monthly_income(0);
		cf.setE_other_income(0);
		cf.setE_c_loan(0);
		cf.setE_c_low_year(0);
		cf.setE_c_low_month(0);
		cf.setE_p_company_name("");
		cf.setE_p_low_year(0);
		cf.setE_p_low_month(0);
		cf.setMobil("No");
		cf.setMobil_kredit("No");
		
		cf.setMotor("No");
		cf.setMotor_kredit("No");
		cf.setAskes("No");
		cf.setDitanggung_kantor("No");
		cf.setAsuransi_pribadi("No");
		cf.setR_mailing_address("");
		cf.setR_address_1("");
		cf.setR_address_2("");
		cf.setR_address_3("");
		cf.setR_address_4("");
		
		cf.setR_city("");
		cf.setR_province("");
		cf.setR_zip_code("");
		cf.setR_kelurahan("");
		cf.setR_kecamatan("");
		cf.setR_home_phone("");
		cf.setR_mobile_phone("");
		cf.setR_email("");
		cf.setR_status("");
		cf.setR_los_year(0);
		
		cf.setR_los_month(0);
		cf.setS_name("");
		cf.setS_id_number("");
		cf.setS_dob("");
		cf.setS_p_agree("");
		
		return cf;
	}
	
	public CustomerForm resetAppForm(CustomerForm cf){
		cf.setA_date_received("");
		cf.setA_facility("");
		cf.setA_purpose("");
		cf.setA_business("");
		cf.setA_media("");
		cf.setA_staff_name("");
		cf.setA_staf_nip("");
		cf.setA_staff_account_no("");
		return cf;
	}

}
