package com.bit.province;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import CCOS.CCOSException;

public class ProvinceController extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		if(request.getSession().getAttribute("is_login")!=null){
			ProvinceForm pf = (ProvinceForm) form;
			ProvinceManager pm = new ProvinceManager();
			Province p = new Province();
			HttpSession session=request.getSession();
			
			String task = pf.getTask();
			String search = pf.getSearch();
			String value = pf.getValue();
			String code = pf.getCode();
			String desc = pf.getDescription();
			String username = (String)session.getAttribute("username");
			String map = "";

			if (task == null||task.equals("load")) {
				pf.setProvList(pm.getAllProvince());
				map = "list";
			} else if (task.equals("search") && search.equals("code")) {
				pf.setProvList(pm.getProvinceByCode(value));
				map = "search";
			} else if (task.equals("search") && search.equals("description")) {
				pf.setProvList(pm.getProvinceByDesc(value));
				map = "search";
			} else if (task.equals("add")) {
				resetProvinceForm(pf);
				map = "add";
			} else if (task.equals("save")) {
				try{
					pm.addProvince(code, desc, username);
					request.setAttribute("add_msg", "Data added.");
					p=pm.getProvinceDetail(code);
					setProvinceForm(pf, p);
					map = "detail";
				}catch(CCOSException e){
					request.setAttribute("error", "ERR["+e.getErrorCode()+"] "+e.getErrorMsg());
					map = "add";
				}
			} else if (task.equals("detail")) {
				p=pm.getProvinceDetail(code);
				setProvinceForm(pf, p);
				map = "detail";
			} else if (task.equals("edit")) {
				p=pm.getProvinceDetail(code);
				setProvinceForm(pf, p);
				map = "edit";
			} else if (task.equals("update")) {
				pm.updateProvince(code, desc, username);
				request.setAttribute("update_msg", "Data updated.");
				p=pm.getProvinceDetail(code);
				setProvinceForm(pf, p);
				map = "detail";
			} else if (task.equals("delete")) {
				pm.deleteProvince(code);
				request.setAttribute("delete_msg", "Data deleted.");
				map = "list";
			} else if (task.equals("logout")) {
				session.invalidate();
				map="login";
			}
			
			if(map.equals("list")){
				resetProvinceForm(pf);
				pf.setProvList(pm.getAllProvince());
			}
			return mapping.findForward(map);
		}else{
			return mapping.findForward("login");
		}
		
	}
	
	
	public ProvinceForm setProvinceForm(ProvinceForm pf, Province p){
		pf.setCode(p.getCode());
		pf.setDescription(p.getDescription());
		pf.setDate_created(p.getDate_created().toString());
		pf.setCreated_by(p.getCreated_by());
		if(p.getDate_updated()!=null){
			pf.setDate_updated(p.getDate_updated().toString());
		}else{
			pf.setDate_updated("");
		}
		if(p.getUpdated_by()!=null){
			pf.setUpdated_by(p.getUpdated_by());
		}else{
			pf.setUpdated_by("");
		}
		return pf;
	}
	
	public ProvinceForm resetProvinceForm(ProvinceForm pf){
		pf.setCode("");
		pf.setDescription("");
		pf.setDate_created("");
		pf.setCreated_by("");
		pf.setDate_updated("");
		pf.setUpdated_by("");
		pf.setSearch("code");
		pf.setValue("");
		return pf;
	}
}
