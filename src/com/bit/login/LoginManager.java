package com.bit.login;

import java.security.MessageDigest;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class LoginManager {

	private static DataSource ds = null;

	public LoginManager() {
		this.SetupConnection();
	}

	public void SetupConnection() {
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		env.put(Context.PROVIDER_URL, "t3://localhost:7001");

		try {
			Context context = new InitialContext(env);
			ds = (DataSource) context.lookup("ds01");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String[] checkLogin(String uid, String pass) throws Exception{
		
		String[] data = { "", "" };
		CallableStatement cs = null;
		Connection conn = null;
		pass=this.getSecurePassword(pass);
		
		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{call LOGIN(?,?,?,?)}");
			cs.setString(1, uid);
			cs.setString(2, pass);
			cs.registerOutParameter(3, java.sql.Types.VARCHAR);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.execute();

			String uname = cs.getString(3);
			String urole = cs.getString(4);

			data[0] = uname;
			data[1] = urole;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
            cs.close();
            conn.close();
        }
		return data;
	}
	
	private String getSecurePassword(String password)
    {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes 
            
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

}
