package com.bit.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginController extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		LoginForm loginForm = (LoginForm) form;
		String task = loginForm.getTask();
		String map = "";
		
		if(task==null||task.equals("load")){
			map="login_page";
		} else if (task.equals("process")){
			String userId = loginForm.getUser_id();
			String password = loginForm.getPassword();

			LoginManager lm = new LoginManager();
			String[] data = { "", "" };
			data = lm.checkLogin(userId, password);

			String username = data[0];
			String role = data[1];

			HttpSession session = request.getSession();
			session.setAttribute("is_login", "true");
			session.setAttribute("username", username);
			session.setAttribute("user_id", userId);

			if (!username.equals("") && role.equals("1")) {
				session.setAttribute("role", "Admin");
				map="home";
				loginForm.setTask("load");
			} else if (!username.equals("") && role.equals("2")) {
				session.setAttribute("role", "Operator");
				map="home";
				loginForm.setTask("load");
			} else {
				loginForm.setError("User ID or password is wrong!");
				map="login_page";
			}
		}
		return mapping.findForward(map);
	}
}
