package com.bit.occupation;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import CCOS.CCOSException;
import oracle.jdbc.OracleTypes;

public class OccupationManager {
	
	private static DataSource ds = null;

	public OccupationManager() {
		this.SetupDataSource();
	}

	public void SetupDataSource() {

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		env.put(Context.PROVIDER_URL, "t3://localhost:7001");
		try {
			Context context = new InitialContext(env);
			ds = (DataSource) context.lookup("ds01");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ArrayList<Occupation> getAllOccupation() throws Exception {

		ArrayList<Occupation> occList = new ArrayList<Occupation>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_ALL_OCCUPATION(?)}");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				Occupation occ = new Occupation();
				occ.setCode(rs.getString("CODE"));
				occ.setDescription(rs.getString("DESCRIPTION"));
				if(rs.getInt("IS_RISKY")==1){
					occ.setIs_risky("Yes");
				}else{
					occ.setIs_risky("No");
				}
				occ.setIls_code(rs.getString("ILS_CODE"));
				occ.setScoreware_code(rs.getString("SCOREWARE_CODE"));
				if(rs.getInt("IS_DEACTIVATED")==1){
					occ.setIs_deactivated("Yes");
				}else{
					occ.setIs_deactivated("No");
				}
				occ.setDate_created(rs.getDate("DATE_CREATED"));
				occ.setCreated_by(rs.getString("CREATED_BY"));
				occ.setDate_updated(rs.getDate("DATE_UPDATED"));
				occ.setUpdated_by(rs.getString("UPDATED_BY"));

				occList.add(occ);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return occList;
	}

	public ArrayList<Occupation> getOccupationByCode(String pcode) throws Exception {

		ArrayList<Occupation> occList = new ArrayList<Occupation>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_OCCUPATION_BY_CODE(?,?)}");
			cs.setString(1, pcode);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				Occupation occ = new Occupation();
				occ.setCode(rs.getString("CODE"));
				occ.setDescription(rs.getString("DESCRIPTION"));
				if(rs.getInt("IS_RISKY")==1){
					occ.setIs_risky("Yes");
				}else{
					occ.setIs_risky("No");
				}
				occ.setIls_code(rs.getString("ILS_CODE"));
				occ.setScoreware_code(rs.getString("SCOREWARE_CODE"));
				if(rs.getInt("IS_DEACTIVATED")==1){
					occ.setIs_deactivated("Yes");
				}else{
					occ.setIs_deactivated("No");
				}
				occ.setDate_created(rs.getDate("DATE_CREATED"));
				occ.setCreated_by(rs.getString("CREATED_BY"));
				occ.setDate_updated(rs.getDate("DATE_UPDATED"));
				occ.setUpdated_by(rs.getString("UPDATED_BY"));
				
				occList.add(occ);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return occList;
	}

	public ArrayList<Occupation> getOccupationByDesc(String desc) throws Exception {

		ArrayList<Occupation> occList = new ArrayList<Occupation>();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_OCCUPATION_BY_DESC(?,?)}");
			cs.setString(1, desc);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				Occupation occ = new Occupation();
				occ.setCode(rs.getString("CODE"));
				occ.setDescription(rs.getString("DESCRIPTION"));
				if(rs.getInt("IS_RISKY")==1){
					occ.setIs_risky("Yes");
				}else{
					occ.setIs_risky("No");
				}
				occ.setIls_code(rs.getString("ILS_CODE"));
				occ.setScoreware_code(rs.getString("SCOREWARE_CODE"));
				if(rs.getInt("IS_DEACTIVATED")==1){
					occ.setIs_deactivated("Yes");
				}else{
					occ.setIs_deactivated("No");
				}
				occ.setDate_created(rs.getDate("DATE_CREATED"));
				occ.setCreated_by(rs.getString("CREATED_BY"));
				occ.setDate_updated(rs.getDate("DATE_UPDATED"));
				occ.setUpdated_by(rs.getString("UPDATED_BY"));
				
				occList.add(occ);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return occList;
	}

	public void addOccupation(String code, String desc, String is_risky, String ils_code, String scoreware_code, String is_deactivated, String created_by) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("CALL ADD_OCCUPATION(?,?,?,?,?,?,?)");
			cs.setString(1, code);
			cs.setString(2, desc);
			if(is_risky.equals("Yes")){
				cs.setInt(3, 1);
			}else{
				cs.setInt(3, 2);
			}
			cs.setString(4, ils_code);
			cs.setString(5, scoreware_code);
			if(is_deactivated.equals("Yes")){
				cs.setInt(6, 1);
			}else{
				cs.setInt(6, 2);
			}
			cs.setString(7, created_by);
			cs.execute();
			System.out.println("Data added");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new CCOSException(e.getErrorCode());
		}finally {
			cs.close();
			conn.close();
		}
	}

	public Occupation getOccupationDetail(String code) throws Exception {

		Occupation occ = new Occupation();
		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL GET_OCCUPATION_DETAIL(?,?)}");
			cs.setString(1, code);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				occ.setCode(rs.getString("CODE"));
				occ.setDescription(rs.getString("DESCRIPTION"));
				if(rs.getInt("IS_RISKY")==1){
					occ.setIs_risky("Yes");
				}else{
					occ.setIs_risky("No");
				}
				occ.setIls_code(rs.getString("ILS_CODE"));
				occ.setScoreware_code(rs.getString("SCOREWARE_CODE"));
				if(rs.getInt("IS_DEACTIVATED")==1){
					occ.setIs_deactivated("Yes");
				}else{
					occ.setIs_deactivated("No");
				}
				occ.setDate_created(rs.getDate("DATE_CREATED"));
				occ.setCreated_by(rs.getString("CREATED_BY"));
				occ.setDate_updated(rs.getDate("DATE_UPDATED"));
				occ.setUpdated_by(rs.getString("UPDATED_BY"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
		return occ;
	}

	public void updateOccupation(String code, String desc, String is_risky, String ils_code, String scoreware_code, String is_deactivated, String updated_by) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("{CALL UPDATE_OCCUPATION(?,?,?,?,?,?,?)}");
			cs.setString(1, code);
			cs.setString(2, desc);
			if(is_risky.equals("Yes")){
				cs.setInt(3, 1);
			}else{
				cs.setInt(3, 2);
			}
			cs.setString(4, ils_code);
			cs.setString(5, scoreware_code);
			if(is_deactivated.equals("Yes")){
				cs.setInt(6, 1);
			}else{
				cs.setInt(6, 2);
			}
			cs.setString(7, updated_by);
			cs.execute();
			System.out.println("Data updated");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
	}

	public void deleteOccupation(String code) throws Exception {

		CallableStatement cs = null;
		Connection conn = null;

		try {
			conn = ds.getConnection();
			cs = conn.prepareCall("CALL DELETE_OCCUPATION(?)");
			cs.setString(1, code);
			cs.execute();
			System.out.println("Data deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cs.close();
			conn.close();
		}
	}

}
