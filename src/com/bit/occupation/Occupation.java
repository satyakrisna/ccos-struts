package com.bit.occupation;

import java.util.Date;

public class Occupation {
	
	private String code;
	private String description;
	private String is_risky;
	private String ils_code;
	private String scoreware_code;
	private String is_deactivated;
	private Date date_created;;
	private String created_by;
	private Date date_updated;;
	private String updated_by;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIs_risky() {
		return is_risky;
	}
	public void setIs_risky(String is_risky) {
		this.is_risky = is_risky;
	}
	public String getIls_code() {
		return ils_code;
	}
	public void setIls_code(String ils_code) {
		this.ils_code = ils_code;
	}
	public String getScoreware_code() {
		return scoreware_code;
	}
	public void setScoreware_code(String scoreware_code) {
		this.scoreware_code = scoreware_code;
	}
	public String getIs_deactivated() {
		return is_deactivated;
	}
	public void setIs_deactivated(String is_deactivated) {
		this.is_deactivated = is_deactivated;
	}
	public Date getDate_created() {
		return date_created;
	}
	public void setDate_created(Date date_created) {
		this.date_created = date_created;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public Date getDate_updated() {
		return date_updated;
	}
	public void setDate_updated(Date date_updated) {
		this.date_updated = date_updated;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	
	
	
}
