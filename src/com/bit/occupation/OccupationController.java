package com.bit.occupation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import CCOS.CCOSException;

public class OccupationController extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if(request.getSession().getAttribute("is_login")!=null){
			OccupationForm of = (OccupationForm) form;
			OccupationManager om = new OccupationManager();
			Occupation o = new Occupation();
			HttpSession session=request.getSession();
			
			String task = of.getTask();
			String search = of.getSearch();
			String value = of.getValue();
			String code = of.getCode();
			String desc = of.getDescription();
			String is_risky = of.getIs_risky();
			String ils_code = of.getIls_code();
			String scoreware_code = of.getScoreware_code();
			String is_deactivated = of.getIs_deactivated();
			String username = (String)session.getAttribute("username");
			String map = "";
			
			if (task == null||task.equals("load")) {
				of.setOccList(om.getAllOccupation());
				map = "list";
			} else if (task.equals("search") && search.equals("code")) {
				of.setOccList(om.getOccupationByCode(value));
				map = "search";
			} else if (task.equals("search") && search.equals("description")) {
				of.setOccList(om.getOccupationByDesc(value));
				map = "search";
			} else if (task.equals("add")) {
				resetOccupationForm(of);
				map = "add";
			} else if (task.equals("save")) {
				try{
					om.addOccupation(code, desc, is_risky, ils_code, scoreware_code, is_deactivated, username);
					request.setAttribute("add_msg", "Data added.");
					o=om.getOccupationDetail(code);
					setOccupationForm(of, o);
					map = "detail";
				}catch(CCOSException e){
					request.setAttribute("error", "ERR["+e.getErrorCode()+"] "+e.getErrorMsg());
					map = "add";
				}
			} else if (task.equals("detail")) {
				o=om.getOccupationDetail(code);
				setOccupationForm(of, o);
				map = "detail";
			} else if (task.equals("edit")) {
				o=om.getOccupationDetail(code);
				setOccupationForm(of, o);
				map = "edit";
			} else if (task.equals("update")) {
				om.updateOccupation(code, desc, is_risky, ils_code, scoreware_code, is_deactivated, username);
				request.setAttribute("update_msg", "Data updated.");
				o=om.getOccupationDetail(code);
				setOccupationForm(of, o);
				map = "detail";
			} else if (task.equals("delete")) {
				om.deleteOccupation(code);
				request.setAttribute("delete_msg", "Data deleted.");
				map = "list";
			} else if (task.equals("logout")) {
				session.invalidate();
				map="login";
			}
			
			if(map.equals("list")){
				resetOccupationForm(of);
				of.setOccList(om.getAllOccupation());
			}
			
			return mapping.findForward(map);
		}else{
			return mapping.findForward("login");
		}
		
	}

	public OccupationForm setOccupationForm(OccupationForm of, Occupation o) {
		of.setCode(o.getCode());
		of.setDescription(o.getDescription());
		of.setIs_risky(o.getIs_risky());
		of.setIls_code(o.getIls_code());
		of.setScoreware_code(o.getScoreware_code());
		of.setIs_deactivated(o.getIs_deactivated());
		of.setDate_created(o.getDate_created().toString());
		of.setCreated_by(o.getCreated_by());
		if (o.getDate_updated() != null) {
			of.setDate_updated(o.getDate_updated().toString());
		} else {
			of.setDate_updated("");
		}
		if (o.getUpdated_by() != null) {
			of.setUpdated_by(o.getUpdated_by());
		} else {
			of.setUpdated_by("");
		}
		return of;
	}

	public OccupationForm resetOccupationForm(OccupationForm of) {
		of.setCode("");
		of.setDescription("");
		of.setIs_risky("No");
		of.setIls_code("");
		of.setScoreware_code("");
		of.setIs_deactivated("No");
		of.setDate_created("");
		of.setCreated_by("");
		of.setDate_updated("");
		of.setUpdated_by("");
		of.setSearch("code");
		of.setValue("");

		return of;
	}
}
