import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.ReportEngine;

public class BIRTGenerator {
	
	public static String repDesign = "C:/Users/bituser/workspace_neon/CCOS/report.rptdesign";
	public static String outputFile= "C:/Users/bituser/workspace_neon/CCOS/output/report.pdf";
	
	public static void main(String [] args){
		ReportEngine engine=null;
		EngineConfig config= new EngineConfig();
		
		try{
			engine=new ReportEngine(config);
			
			IReportRunnable design=null;
			design=engine.openReportDesign(repDesign);
			
			IRunAndRenderTask task=engine.createRunAndRenderTask(design);
			task.setParameterValue("cabang1", "0970");
			task.setParameterValue("Cabang2", "0969");
			task.validateParameters();
			
			final HTMLRenderOption HTML_OPTION=new HTMLRenderOption();
			HTML_OPTION.setOutputFileName(outputFile);
			HTML_OPTION.setOutputFormat("pdf");
			task.setRenderOption(HTML_OPTION);
			task.run();
			task.close();
			engine.destroy();
		}catch(final Exception e){
			e.printStackTrace();
		}finally{
			Platform.shutdown();
		}
	}
}
