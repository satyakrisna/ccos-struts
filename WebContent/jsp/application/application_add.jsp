<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<title>CCOS - City</title>

</head>
<body onload="startTime()">

	<script language="JavaScript" type="text/JavaScript">
		function goToSaveApp(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
		
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	<html:form action="/customer" method="post">
	<html:hidden property="task" value="load" />
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a> 
		<a class="menu">Welcome, <core:out value="${sessionScope.username}"/></a>
	</div>
	<div class="sidebar">
		<ul id="sidemenu">
			<li><a href="home.do">Home</a></li>
			<li><a class="active" href="customer.do?task=load">New Aplication</a></li>
			<li><a href="">Report</a></li>
			<li><a href="javascript:goLogout('logout')">Logout</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="container">
			<h3 class="title">Add New Application</h3>
			<logic:notEqual name="error" value="">
				<p style="color:red"><core:out value="${requestScope.error}" /></p>
			</logic:notEqual>
				<fieldset>
					<legend>Application Detail:</legend>
					<div class="row">
						<div class="col-25">
							<label for="value">Referral Branch</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_ref_branch" maxlength="40" readonly="true" value="0960 - Unit Bisnis Kartu Kredit"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Date Received</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_date_received" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Facility</label>
						</div>
						<div class="col-75">
							<html:select property="a_facility">
								<html:option value="PL Asuransi Jiwa">PL Asuransi Jiwa</html:option>
								<html:option value="PL Asuransi Jiwa + PHK">PL Asuransi Jiwa + PHK</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Application Purpose</label>
						</div>
						<div class="col-75">
							<html:select property="a_purpose">
								<html:option value="Sekolah">Sekolah</html:option>
								<html:option value="Liburan">Liburan</html:option>
								<html:option value="Modal Kerja">Modal Kerja</html:option>
								<html:option value="Lainnya">Lainnya</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Business Source</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_business" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Media Channel</label>
						</div>
						<div class="col-75">
							<html:select property="a_media">
								<html:option value="TV">TV</html:option>
								<html:option value="Surat Kabar">Surat Kabar</html:option>
								<html:option value="Internet">Internet</html:option>
								<html:option value="Cabang">Cabang</html:option>
								<html:option value="Lainnya">Lainnya</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Fee Branch</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_fee" maxlength="40" readonly="true" value="0960 - Unit Bisnis Kartu Kredit"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Provision Branch</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_provision" maxlength="40" readonly="true" value="0960 - Unit Bisnis Kartu Kredit"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">KCKK Branch</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_kckk" maxlength="40" readonly="true" value="0960 - Unit Bisnis Kartu Kredit"/>
						</div>
					</div>
					<div class="row">
						<p>Staff Introducer Information</p>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Staff Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_staff_name" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Staff NIP No.</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_staf_nip" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Staff Branch</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_staff_branch" maxlength="40" readonly="true" value="0960 - Unit Bisnis Kartu Kredit"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Staff Account No.</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="a_staff_account_no" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<br><a class="button" href="javascript:goToSaveApp('saveapp')">Save</a>
					</div>
				</fieldset>
		</div>
	</div>
	</html:form>
	<script type="text/javascript" src="js/ccos.js"></script>
	<script>
	$(document).ready(function() {
	    $('.select2').select2();
	});
	</script>
</body>
</html>