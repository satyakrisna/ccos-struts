<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<script language = "JavaScript" src = "js/validator.js"></script>
<script language = "JavaScript" src = "js/js_validator.js"></script>
<html:javascript scriptLanguage="javascript" formName="occupationForm" method="validateForm" staticJavascript="false" dynamicJavascript="true" page="0"/>
	
<title>CCOS - Occupation</title>

</head>
<body onload="startTime()">
	<script language="JavaScript" type="text/JavaScript">
		function goToUpdate(task) {
			if (!validateForm(document.forms[0])){
				return;
				
			}
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
		
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	<html:form action="/occupation" method="post">
	<html:hidden property="task" value="load" />
	<html:hidden property="code"/>
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a>
		<a class="menu">Welcome, <core:out value="${sessionScope.username}" /></a>
	</div>
	<div class="sidebar">
		<ul id="sidemenu">
			<li><a href="home.do">Home</a></li>
			<li><a class="caret active">Maintenance</a>
				<ul class="nested">
					<li><a href="province.do?task=load">Province</a></li>
					<li><a href="city.do?task=load">City</a></li>
					<li><a class="active" href="occupation.do?task=load">Occupation</a></li>
				</ul></li>
			<li><a href="user.do?task=load">User</a></li>
			<li><a href="javascript:goLogout('logout')">Logout</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="container">
			<h3 class="title">Edit Occupation</h3>
				<fieldset>
					<legend>Occupation Detail:</legend>
							<div class="row">
						<div class="col-25">
							<label for="value">Code</label>
						</div>
						<div class="col-75">
							<html:text name="occupationForm" property="code" maxlength="40" disabled="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Description</label>
						</div>
						<div class="col-75">
							<html:text name="occupationForm" property="description" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="search">Is Risky</label>
						</div>
						<div class="col-75">
							<html:checkbox property="is_risky" value="Yes"></html:checkbox>
							<html:hidden property="is_risky" value="No"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">ILS Code</label>
						</div>
						<div class="col-75">
							<html:text name="occupationForm" property="ils_code" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Scoreware Code</label>
						</div>
						<div class="col-75">
							<html:text name="occupationForm" property="scoreware_code" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="search">Is Deactivated</label>
						</div>
						<div class="col-75">
							<html:checkbox property="is_deactivated" value="Yes"></html:checkbox>
							<html:hidden property="is_deactivated" value="No"/>
						</div>
					</div>
							<div class="row">
								<br> <a class="button" href="javascript:goToUpdate('update','<bean:write name="occupationForm" property="code"/>')">Update</a>
							</div>
				</fieldset>
		</div>
	</div>
	</html:form>
	<script type="text/javascript" src="js/ccos.js"></script>
</body>
</html>