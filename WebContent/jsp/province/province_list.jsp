<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<title>CCOS - Province</title>

</head>
<body onload="startTime()">

	<script language="JavaScript" type="text/JavaScript">
		function goToSearch(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
			
		}
		function goToAdd(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
		function flyToPage(task, code )
	      {
			document.forms[0].task.value = task;
			document.forms[0].code.value = code;
			document.forms[0].submit();
		}
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	
	<html:form action="/province" method="post">
	<html:hidden name="provinceForm" property="task" value="load" />
	<html:hidden name="provinceForm" property="code"/>
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a>
		<a class="menu">Welcome, <core:out value="${sessionScope.username}" /></a>
	</div>
	<div class="sidebar">
		<ul id="sidemenu">
			<li><a href="home.do">Home</a></li>
			<li><a class="caret active">Maintenance</a>
				<ul class="nested">
					<li><a class="active" href="province.do?task=load">Province</a></li>
					<li><a href="city.do?task=load">City</a></li>
					<li><a href="occupation.do?task=load">Occupation</a></li>
				</ul></li>
			<li><a href="user.do?task=load">User</a></li>
			<li><a href="javascript:goLogout('logout')">Logout</a></li>
		</ul>
	</div>
		<div class="content">
			<div class="container">
				<h3 class="title">Search Province</h3>
				<fieldset>
					<legend>Search Criteria:</legend>
					<div class="row">
						<div class="col-25">
							<label for="search">Search By</label>
						</div>
						<div class="col-75">
							<html:radio name="provinceForm" property="search" value="code" />
							<label for="code">Code</label><br>
							<html:radio name="provinceForm" property="search"
								value="description" />
							<label for="desc">Description</label>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Enter Value</label>
						</div>
						<div class="col-75">
							<html:text name="provinceForm" maxlength="40" property="value"
								/>
						</div>
					</div>
					<div class="row">
						<br> <a class="button" href="javascript:goToAdd('add')">Add</a>
						<a class="button" href="javascript:goToSearch('search')">Search</a>
					</div>
				</fieldset>
			</div>
			<div class="container">
				<h3 class="title">Province List</h3>
				<logic:notEqual name="delete_msg" value="">
					<p style="color:green"><core:out value="${requestScope.delete_msg}" /></p>
				</logic:notEqual>
				<table class="data">
					<thead>
					<tr>
						<th class="theader">No.</th>
						<th class="theader">Code</th>
						<th class="theader">Description</th>
					</tr>
					</thead>
					<tbody>
					<logic:notEmpty name="provinceForm" property="provList">
						<logic:iterate id="i" name="provinceForm" property="provList" indexId="index">
							<html:form action="/province" method="post">
								<tr>
									<td><core:out value="${index + 1}" /></td>
									<td>
										<a href="javascript:flyToPage('detail', '<bean:write name="i" property="code" />')">
											<bean:write name="i" property="code" />
										</a>
									</td>
									<td>
										<bean:write name="i" property="description" />
									</td>
								</tr>
							</html:form>
						</logic:iterate>
					</logic:notEmpty>
					</tbody>
				</table>
			</div>
		</div>
	</html:form>
	<script type="text/javascript" src="js/ccos.js"></script>
	<script>
	$(document).ready(function() {
		$('.data').DataTable({
			bFilter : false
		});
	});
	</script>
</body>
</html>