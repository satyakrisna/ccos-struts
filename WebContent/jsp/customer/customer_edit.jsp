<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<title>CCOS - Customer</title>

<style>
input[type=text], select, textarea {
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	resize: vertical;
	width: 30%;
}
.spouse_info{
	display:none;
}
</style>

</head>
<body onload="startTime()">

	<script language="JavaScript" type="text/JavaScript">
		function goToUpdate(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
		
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	<html:form action="/customer" method="post">
	<html:hidden property="task" value="load" />
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a> 
		<a class="menu">Welcome, <core:out value="${sessionScope.username}"/></a>
	</div>
	<div class="sidebar">
		<ul id="sidemenu">
			<li><a href="home.do">Home</a></li>
			<li><a class="active" href="customer.do?task=load">New Aplication</a></li>
			<li><a href="report.do">Report</a></li>
			<li><a href="javascript:goLogout('logout')">Logout</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="container">
			<h3 class="title">Edit Customer</h3>
			<logic:notEqual name="error" value="">
				<p style="color:red"><core:out value="${requestScope.error}" /></p>
			</logic:notEqual>
				<fieldset>
					<legend>Personal Information</legend>
					<div class="row">
						<div class="col-25">
							<label for="value">Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="name" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Fullname</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="fullname" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">ID Type</label>
						</div>
						<div class="col-75">
							<html:select property="id_type">
								<html:option value="KTP">1 - KTP</html:option>
								<html:option value="SIM">2 - SIM</html:option>
								<html:option value="Passport">3 - Passport</html:option>
							</html:select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-25">
							<label for="value">ID Number</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="id_number" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">ID Expiry Date</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="id_exp_date" maxlength="40" />
							<p>DD/MM/YYYY</p>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Gender</label>
						</div>
						<div class="col-75">
							<html:select property="gender">
								<html:option value="Pria">1 - Pria</html:option>
								<html:option value="Wanita">2 - Wanita</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Place of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="pob" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">City of Birth</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="cob">
								<logic:notEmpty name="customerForm" property="cityList">
									<logic:iterate id="i" name="customerForm" property="cityList" indexId="index">
										<bean:define id="p_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=p_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Province of Birth</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="prob">
								<logic:notEmpty name="customerForm" property="provList">
									<logic:iterate id="i" name="customerForm" property="provList" indexId="index">
										<bean:define id="p_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=p_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Date of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="dob" maxlength="40" />
							<label>DD/MM/YYYY</label>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Age</label>
						</div>
						<div class="col-75">
							<input type="text"  id="age_year" maxlength="2" />
							<label for="value">year(s)</label>
							<input type="text" id="age_month" maxlength="2" />
							<label for="value">month(s)</label>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Marital Status</label>
						</div>
						<div class="col-75">
							<html:select styleId="m_status" property="m_status">
								<html:option value="Single">01 - Single</html:option>
								<html:option value="Married">02 - Married</html:option>
								<html:option value="Widowed">03 - Widowed</html:option>
								<html:option value="Divorced">04 - Divorced</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Number of Dependents</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="no_dep" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Level of Education</label>
						</div>
						<div class="col-75">
							<html:select property="edu_level">
								<html:option value="SD">01 - SD</html:option>
								<html:option value="SMP">02 - SMP</html:option>
								<html:option value="SMA">03 - SMA</html:option>
								<html:option value="D3">04 - D3</html:option>
								<html:option value="S1">05 - S1</html:option>
								<html:option value="S2">06 - S2</html:option>
								<html:option value="S3">07 - S3</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Mother's Maiden Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="mother_name" maxlength="40" />
						</div>
					</div>
					<div class="row spouse_info">
						<p style="color:#33589C">Spouse Information</p>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Spouse Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="s_name" maxlength="40" />
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">ID No</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="s_id_number" maxlength="40" />
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Date of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="s_dob" maxlength="40" />
							<label>DD/MM/YYYY</label>
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Age</label>
						</div>
						<div class="col-75">
							<input type="text"  id="s_age_year" maxlength="2" />
							<label for="value">year(s)</label>
							<input type="text" id="s_age_month" maxlength="2" />
							<label for="value">month(s)</label>
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Prenuptial Agreement</label>
						</div>
						<div class="col-75">
							<html:select property="s_p_agree">
								<html:option value="Yes">Yes</html:option>
								<html:option value="No">No</html:option>
							</html:select>
						</div>
					</div>
					
				</fieldset>
				<fieldset>
					<legend>Residence Information</legend>
					<div class="row">
						<div class="col-25">
							<label for="value">Mailing Address</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_mailing_address" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Residence Address</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_address_1" maxlength="40" /><br>
							<html:text name="customerForm" property="r_address_2" maxlength="40" /><br>
							<html:text name="customerForm" property="r_address_3" maxlength="40" /><br>
							<html:text name="customerForm" property="r_address_4" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">City</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="r_city">
								<logic:notEmpty name="customerForm" property="cityList">
									<logic:iterate id="i" name="customerForm" property="cityList" indexId="index">
										<bean:define id="p_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=p_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Province</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="r_province">
								<logic:notEmpty name="customerForm" property="provList">
									<logic:iterate id="i" name="customerForm" property="provList" indexId="index">
										<bean:define id="p_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=p_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Zip Code</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_zip_code" maxlength="5" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Kelurahan</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_kelurahan" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Kecamatan</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_kecamatan" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Home Phone No.</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_home_phone" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Mobile Phone No.</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_mobile_phone" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Email</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_email" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Residence Status</label>
						</div>
						<div class="col-75">
							<html:select property="r_status">
								<html:option value="Owner">00 - Owner</html:option>
								<html:option value="Parent">01 - Parent</html:option>
								<html:option value="Rent">02 - Rent</html:option>
								<html:option value="Relatives">03 - Relatives</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Length Of Stay</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_los_year" maxlength="2" />
							<label for="value">year(s)</label>
							<html:text name="customerForm" property="r_los_month" maxlength="2" />
							<label for="value">month(s)</label>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Employment Information</legend>
					<div class="row">
						<p style="color:#33589C">Current Employment</p>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Company Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_company_name" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Employment Type</label>
						</div>
						<div class="col-75">
							<html:select property="e_emp_type">
								<html:option value="Professional">1 - Professional</html:option>
								<html:option value="Wiraswasta">2 - Wiraswasta</html:option>
								<html:option value="Karyawan">3 - Karyawan</html:option>
								<html:option value="Ibu Rumah Tangga">4 - Ibu Rumah Tangga</html:option>
								<html:option value="PNS">5 - PNS</html:option>
								<html:option value="Lainnya">6 - Lainnya</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Office Address</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_address_1" maxlength="40" /><br>
							<html:text name="customerForm" property="e_address_2" maxlength="40" /><br>
							<html:text name="customerForm" property="e_address_3" maxlength="40" /><br>
							<html:text name="customerForm" property="e_address_4" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">City</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="e_city">
								<logic:notEmpty name="customerForm" property="cityList">
									<logic:iterate id="i" name="customerForm" property="cityList" indexId="index">
										<bean:define id="p_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=p_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Province</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="e_province">
								<logic:notEmpty name="customerForm" property="provList">
									<logic:iterate id="i" name="customerForm" property="provList" indexId="index">
										<bean:define id="c_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=c_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Zip Code</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_zip_code" maxlength="5" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Office Phone</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_office_phone" maxlength="40" />
							<label>Extension</label>
							<html:text name="customerForm" property="e_extension" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Occupation</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="e_occupation">
								<logic:notEmpty name="customerForm" property="occList">
									<logic:iterate id="i" name="customerForm" property="occList" indexId="index">
										<bean:define id="o_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=o_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Designation</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_designation" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Job Sector</label>
						</div>
						<div class="col-75">
							<html:select property="e_job_sector">
								<html:option value="Retail">409940  - Retail</html:option>
								<html:option value="Property">889001  - Property</html:option>
								<html:option value="Banking">848601  - Banking</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Tax Indicator</label>
						</div>
						<div class="col-75">
							<html:select property="e_tax_indicator">
								<html:option value="NPWP atas nama debitur">Y - NPWP atas nama debitur</html:option>
								<html:option value="Tidak memiliki NPWP">N - Tidak memiliki NPWP</html:option>
								<html:option value="Istri pinjam NPWP suami">2  - Istri pinjam NPWP suami</html:option>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Tax ID (Personal)</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_tax_id" maxlength="16" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Monthly Income</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_monthly_income" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Other Income</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_other_income" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Current Loan Repayment</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_c_loan" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Curret Length of Work</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_c_low_year" maxlength="2" />
							<label for="value">year(s)</label>
							<html:text name="customerForm" property="e_c_low_month" maxlength="2" />
							<label for="value">month(s)</label>
						</div>
					</div>
					<div class="row">
						<p style="color:#33589C">Previous Employment</p>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Previous Company Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_p_company_name" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Previouse Length of Work</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_p_low_year" maxlength="2" />
							<label for="value">year(s)</label>
							<html:text name="customerForm" property="e_p_low_month" maxlength="2" />
							<label for="value">month(s)</label>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Others</legend>
					<div class="row">
						<div class="col-25">
							<label for="search">Vehicle Ownership</label>
						</div>
						<div class="col-75">
							<html:checkbox property="mobil" value="Yes">Mobil</html:checkbox>
							<html:hidden property="mobil" value="No"/>
							<html:checkbox property="mobil_kredit" value="Yes">Mobil Dalam Masa Kredit</html:checkbox>
							<html:hidden property="mobil_kredit" value="No"/>
							<html:checkbox property="motor" value="Yes">Motor</html:checkbox>
							<html:hidden property="motor" value="No"/>
							<html:checkbox property="motor_kredit" value="Yes">Motor Dalam Masa Kredit</html:checkbox>
							<html:hidden property="motor_kredit" value="No"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="search">Heatlh Insurance Ownership</label>
						</div>
						<div class="col-75">
							<html:checkbox property="askes" value="Yes">Askes</html:checkbox>
							<html:hidden property="askes" value="No"/>
							<html:checkbox property="ditanggung_kantor" value="Yes">Ditanggung Kantor</html:checkbox>
							<html:hidden property="ditanggung_kantor" value="No"/>
							<html:checkbox property="asuransi_pribadi" value="Yes">Asuransi Pribadi</html:checkbox>
							<html:hidden property="asuransi_pribadi" value="No"/>
						</div>
					</div>
				</fieldset>
				<div class="row">
						<br><a class="button" href="javascript:goToUpdate('update')">Save</a>
				</div>
		</div>
	</div>
	</html:form>
	<script type="text/javascript" src="js/ccos.js"></script>
	<script>
	$(document).ready(function() {
	    $('.select2').select2();
	});
	
	$('#m_status').on('change', function () {
        $(".spouse_info").toggle(this.value == 'Married');

    });
	</script>
</body>
</html>