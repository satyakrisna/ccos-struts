<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script language="JavaScript" src="js/validator.js"></script>
<script language="JavaScript" src="js/js_validator.js"></script>
<html:javascript scriptLanguage="javascript" formName="customerForm" method="validateForm" staticJavascript="false" dynamicJavascript="true" page="0" />

<title>CCOS - Customer</title>

<style>
input[type=text], select, textarea {
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	resize: vertical;
	width: 30%;
}
.col-25{
	background-color: #33589C;
	border-radius: 4px;
}
.col-25 label{
	margin-left:20px;
	color:white;
}
.spouse_info{
	display:none;
}
</style>

</head>
<body onload="startTime()">

	<script language="JavaScript" type="text/JavaScript">
		function goTo(task, id_number) {
			document.forms[0].task.value = task;
			document.forms[0].id_number.value = id_number;
			document.forms[0].submit();
		}
		
		function goToAddApp(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	<html:form action="/customer" method="post">
	<html:hidden property="task" value="load" />
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a> 
		<a class="menu">Welcome, <core:out value="${sessionScope.username}"/></a>
	</div>
	<div class="sidebar">
		<ul id="sidemenu">
			<li><a href="home.do">Home</a></li>
			<li><a class="active" href="customer.do?task=load">New Aplication</a></li>
			<li><a href="report.do">Report</a></li>
			<li><a href="javascript:goLogout('logout')">Logout</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="container">
				<h3 class="title">Customer Application List <a class="button" href="javascript:goToAddApp('app')">Add New Application</a></h3>
				<logic:notEqual name="delete_msg" value="">
					<p style="color:green"><core:out value="${requestScope.delete_msg}" /></p>
				</logic:notEqual>
				<table class="data">
					<thead>
					<tr>
						<th class="theader">Ref No.</th>
						<th class="theader">Date Created</th>
						<th class="theader">Creator</th>
						<th class="theader">Status</th>
						<th class="theader">Hold By</th>
					</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="customerForm" property="appList">
						<logic:iterate id="i" name="customerForm" property="appList" indexId="index">
								<tr>
									<td>
										<bean:write name="i" property="ref_no" />
									</td>
									<td>
										<bean:write name="i" property="date_created" />
									</td>
									<td>
										<bean:write name="i" property="creator" />
									</td>
									<td>
										<bean:write name="i" property="status" />
									</td>
									<td>
										<bean:write name="i" property="hold_by" />
									</td>
								</tr>
						</logic:iterate>
					</logic:notEmpty>
					</tbody>
				</table>
			</div>
		<div class="container">
			<h3 class="title">Detail Customer</h3>
			<logic:notEqual name="error" value="">
				<p style="color:red"><core:out value="${requestScope.error}" /></p>
			</logic:notEqual>
				<fieldset>
					<legend>Personal Information</legend>
					<div class="row">
						<div class="col-25">
							<label for="value">Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="name" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Fullname</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="fullname" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">ID Type</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="id_type" maxlength="40" readonly="true"/>
						</div>
					</div>
					
					<div class="row">
						<div class="col-25">
							<label for="value">ID Number</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="id_number" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">ID Expiry Date</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="id_exp_date" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Gender</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="gender" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Place of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="pob" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">City of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="cob" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Province of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="prob" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Date of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="dob" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Age</label>
						</div>
						<div class="col-75">
							<input type="text"  id="age_year" maxlength="2" readonly/>
							<label for="value">year(s)</label>
							<input type="text" id="age_month" maxlength="2" readonly/>
							<label for="value">month(s)</label>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Marital Status</label>
						</div>
						<div class="col-75">
							<html:text styleId="m_status" name="customerForm" property="m_status" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Number of Dependents</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="no_dep" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Level of Education</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="edu_level" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Mother's Maiden Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="mother_name" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row spouse_info">
						<p style="color:#33589C">Spouse Information</p>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Spouse Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="s_name" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">ID No</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="s_id_number" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Date of Birth</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="s_dob" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Age</label>
						</div>
						<div class="col-75">
							<input type="text"  id="s_age_year" maxlength="2" readonly/>
							<label for="value">year(s)</label>
							<input type="text" id="s_age_month" maxlength="2" readonly/>
							<label for="value">month(s)</label>
						</div>
					</div>
					<div class="row spouse_info">
						<div class="col-25">
							<label for="value">Prenuptial Agreement</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="s_p_agree" maxlength="40" readonly="true"/>
						</div>
					</div>
					
				</fieldset>
				<fieldset>
					<legend>Residence Information</legend>
					<div class="row">
						<div class="col-25">
							<label for="value">Mailing Address</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_mailing_address" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Residence Address</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_address_1" maxlength="40" readonly="true"/><br>
							<html:text name="customerForm" property="r_address_2" maxlength="40" readonly="true"/><br>
							<html:text name="customerForm" property="r_address_3" maxlength="40" readonly="true"/><br>
							<html:text name="customerForm" property="r_address_4" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">City</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_city" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Province</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_province" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Zip Code</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_zip_code" maxlength="5" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Kelurahan</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_kelurahan" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Kecamatan</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_kecamatan" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Home Phone No.</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_home_phone" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Mobile Phone No.</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_mobile_phone" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Email</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_email" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Residence Status</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_status" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Length Of Stay</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="r_los_year" maxlength="2" readonly="true"/>
							<label for="value">year(s)</label>
							<html:text name="customerForm" property="r_los_month" maxlength="2" readonly="true"/>
							<label for="value">month(s)</label>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Employment Information</legend>
					<div class="row">
						<p style="color:#33589C">Current Employment</p>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Company Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_company_name" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Employment Type</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_emp_type" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Office Address</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_address_1" maxlength="40" readonly="true"/><br>
							<html:text name="customerForm" property="e_address_2" maxlength="40" readonly="true"/><br>
							<html:text name="customerForm" property="e_address_3" maxlength="40" readonly="true"/><br>
							<html:text name="customerForm" property="e_address_4" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">City</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_city" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Province</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_province" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Zip Code</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_zip_code" maxlength="5" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Office Phone</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_office_phone" maxlength="40" readonly="true"/>
							<label>Extension</label>
							<html:text name="customerForm" property="e_extension" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Occupation</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_occupation" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Designation</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_designation" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Job Sector</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_job_sector" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Tax Indicator</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_tax_indicator" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Tax ID (Personal)</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_tax_id" maxlength="16" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Monthly Income</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_monthly_income" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Other Income</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_other_income" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Current Loan Repayment</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_c_loan" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Curret Length of Work</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_c_low_year" maxlength="2" readonly="true"/>
							<label for="value">year(s)</label>
							<html:text name="customerForm" property="e_c_low_month" maxlength="2" readonly="true"/>
							<label for="value">month(s)</label>
						</div>
					</div>
					<div class="row">
						<p style="color:#33589C">Previous Employment</p>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Previous Company Name</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_p_company_name" maxlength="40" readonly="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Previouse Length of Work</label>
						</div>
						<div class="col-75">
							<html:text name="customerForm" property="e_p_low_year" maxlength="2" readonly="true"/>
							<label for="value">year(s)</label>
							<html:text name="customerForm" property="e_p_low_month" maxlength="2" readonly="true"/>
							<label for="value">month(s)</label>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Others</legend>
					<div class="row">
						<div class="col-25">
							<label for="search">Vehicle Ownership</label>
						</div>
						<div class="col-75">
							<html:checkbox property="mobil" value="Yes" disabled="true">Mobil</html:checkbox>
							<html:hidden property="mobil" value="No"/>
							<html:checkbox property="mobil_kredit" value="Yes" disabled="true">Mobil Dalam Masa Kredit</html:checkbox>
							<html:hidden property="mobil_kredit" value="No"/>
							<html:checkbox property="motor" value="Yes" disabled="true">Motor</html:checkbox>
							<html:hidden property="motor" value="No"/>
							<html:checkbox property="motor_kredit" value="Yes" disabled="true">Motor Dalam Masa Kredit</html:checkbox>
							<html:hidden property="motor_kredit" value="No"/>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="search">Heatlh Insurance Ownership</label>
						</div>
						<div class="col-75">
							<html:checkbox property="askes" value="Yes" disabled="true">Askes</html:checkbox>
							<html:hidden property="askes" value="No"/>
							<html:checkbox property="ditanggung_kantor" value="Yes" disabled="true">Ditanggung Kantor</html:checkbox>
							<html:hidden property="ditanggung_kantor" value="No"/>
							<html:checkbox property="asuransi_pribadi" value="Yes" disabled="true">Asuransi Pribadi</html:checkbox>
							<html:hidden property="asuransi_pribadi" value="No"/>
						</div>
					</div>
				</fieldset>
				<div class="row">
						<br><a class="button" href="javascript:goTo('edit', '<bean:write name="customerForm" property="id_number" />')">Edit</a>
							<a style="background-color: red" class="button" href="javascript:goTo('delete', '<bean:write name="customerForm" property="id_number" />')">Delete</a>
				</div>
		</div>
	</div>
	</html:form>
	<script type="text/javascript" src="js/ccos.js"></script>
	<script>
	$(document).ready(function() {
	    $('.select2').select2();
	    if($('#m_status').val()=="Married"){
	    	$(".spouse_info").show();
	    }
	});
	
	$('#m_status').on('change', function () {
        $(".spouse_info").toggle(this.value == 'Married');

    });
	</script>
</body>
</html>