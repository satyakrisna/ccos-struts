<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script language="JavaScript" src="js/validator.js"></script>
<script language="JavaScript" src="js/js_validator.js"></script>
<html:javascript scriptLanguage="javascript" formName="cityForm" method="validateForm" staticJavascript="false" dynamicJavascript="true" page="0" />

<title>CCOS - City</title>

</head>
<body onload="startTime()">

	<script language="JavaScript" type="text/JavaScript">
		function goToSave(task) {
			if (!validateForm(document.forms[0])) {
				return;

			}
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
		
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	<html:form action="/city" method="post">
	<html:hidden property="task" value="load" />
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a> 
		<a class="menu">Welcome, <core:out value="${sessionScope.username}"/></a>
	</div>
	<div class="sidebar">
		<ul id="sidemenu">
			<li><a href="home.do?task=load">Home</a></li>
			<li><a class="caret active">Maintenance</a>
				<ul class="nested">
					<li><a href="province.do?task=load">Province</a></li>
					<li><a class="active" href="city.do?task=load">City</a></li>
					<li><a href="occupation.do?task=load">Occupation</a></li>
				</ul></li>
			<li><a href="user.do?task=load">User</a></li>
			<li><a href="javascript:goLogout('logout')">Logout</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="container">
			<h3 class="title">Add New City</h3>
			<logic:notEqual name="error" value="">
				<p style="color:red"><core:out value="${requestScope.error}" /></p>
			</logic:notEqual>
				<fieldset>
					<legend>City Detail:</legend>
					<div class="row">
						<div class="col-25">
							<label for="value">Code</label>
						</div>
						<div class="col-75">
							<html:text name="cityForm" property="code" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Province</label>
						</div>
						<div class="col-75">
							<html:select styleClass="select2" property="p_code">
								<logic:notEmpty name="cityForm" property="provList">
									<logic:iterate id="i" name="cityForm" property="provList" indexId="index">
										<bean:define id="p_code" name="i" property="code" type="java.lang.String"/>
										<html:option value="<%=p_code%>"><bean:write name="i" property="code" /> - <bean:write name="i" property="description" /></html:option>
									</logic:iterate>
								</logic:notEmpty>
							</html:select>
						</div>
					</div>
					<div class="row">
						<div class="col-25">
							<label for="value">Description</label>
						</div>
						<div class="col-75">
							<html:text name="cityForm" property="description" maxlength="40" />
						</div>
					</div>
					<div class="row">
						<br><a class="button" href="javascript:goToSave('save')">Save</a>
					</div>
				</fieldset>
		</div>
	</div>
	</html:form>
	<script type="text/javascript" src="js/ccos.js"></script>
	<script>
	$(document).ready(function() {
	    $('.select2').select2();
	});
	</script>
</body>
</html>