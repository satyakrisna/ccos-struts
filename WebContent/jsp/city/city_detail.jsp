<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<title>CCOS - City</title>

</head>
<body onload="startTime()">
	<script language="JavaScript" type="text/JavaScript">
		function goTo(task, code) {
			document.forms[0].task.value = task;
			document.forms[0].code.value = code;
			document.forms[0].submit();
		}
		
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	
	<html:form action="/city" method="post">
	<html:hidden property="task" value="load" />
	<html:hidden name="cityForm" property="code" />
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a>
		<a class="menu">Welcome, <core:out value="${sessionScope.username}" /></a>
	</div>

	<div class="sidebar">
		<ul id="sidemenu">
			<li><a href="home.do">Home</a></li>
			<li><a class="caret active">Maintenance</a>
				<ul class="nested">
					<li><a href="province.do?task=load">Province</a></li>
					<li><a class="active" href="city.do?task=load">City</a></li>
					<li><a href="occupation.do?task=load">Occupation</a></li>
				</ul></li>
			<li><a href="user.do?task=load">User</a></li>
			<li><a href="javascript:goLogout('logout')">Logout</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="container">
			<logic:notEqual name="add_msg" value="">
				<p style="color:green"><core:out value="${requestScope.add_msg}" /></p>
			</logic:notEqual>
			<h3 class="title">City Detail</h3>
			<logic:notEqual name="update_msg" value="">
				<p style="color:green"><core:out value="${requestScope.update_msg}" /></p>
			</logic:notEqual>
				<table>
					<tr>
						<th class="theader">Code</th>
						<td><bean:write name="cityForm" property="code" /></td>
					</tr>
					<tr>
						<th class="theader">Description</th>
						<td><bean:write name="cityForm" property="description" /></td>
					</tr>
					<tr>
						<th class="theader">Province</th>
						<td><bean:write name="cityForm" property="province" /></td>
					</tr>
					<tr>
						<th class="theader">Date Created</th>
						<td><bean:write name="cityForm" property="date_created" /></td>
					</tr>
					<tr>
						<th class="theader">Created By</th>
						<td><bean:write name="cityForm" property="created_by" /></td>
					</tr>
					<tr>
						<th class="theader">Date Updated</th>
						<td><bean:write name="cityForm" property="date_updated" /></td>
					</tr>
					<tr>
						<th class="theader">Updated By</th>
						<td><bean:write name="cityForm" property="updated_by" /></td>
					</tr>
					<tr>
						<th class="theader">Action</th>
						<td>
							<a class="button" href="javascript:goTo('edit', '<bean:write name="cityForm" property="code" />')">Edit</a>
							<a style="background-color: red" class="button" href="javascript:goTo('delete', '<bean:write name="cityForm" property="code" />')">Delete</a>
						</td>
					</tr>
				</table>
		</div>
	</div>
	</html:form>
	<script type="text/javascript" src="js/ccos.js"></script>
</body>
</html>