<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/ccos.css">

<title>CCOS - Home</title>

</head>
<body onload="startTime()">
	<script language="JavaScript" type="text/JavaScript">
		function goLogout(task) {
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	<div class="topnav" id="myTopnav">
		<a href="home.do"><b>CCOS - BCA Personal Loan</b></a> 
		<a class="menu" id="time"></a>
		<a class="menu">Welcome, <core:out value="${sessionScope.username}" /></a>
	</div>
	<div class="sidebar">
		<ul id="sidemenu">
			<li><a class="active" href="home.do">Home</a></li>
			<li><a href="customer.do?task=load">New Aplication</a></li>
			<li><a href="report.do">Report</a></li>
			<html:form action="/home" method="post">
				<html:hidden name="homeForm" property="task" value="load" />
				<li><a href="javascript:goLogout('logout')">Logout</a></li>
			</html:form>
		</ul>
	</div>
	<div class="content">
		<h3>Generate Report</h3>
		<a href="genrep.do" class="button">Generate Report</a>
		<logic:notEqual name="msg" value="">
					<p style="color:green"><core:out value="${requestScope.msg}" /></p>
		</logic:notEqual>
	</div>
	<script type="text/javascript" src="js/ccos.js"></script>
</body>
</html>