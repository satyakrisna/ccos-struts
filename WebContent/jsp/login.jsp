<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" type="text/css" href="css/login.css">

<script language="JavaScript" src="js/validator.js"></script>
<script language="JavaScript" src="js/js_validator.js"></script>
<html:javascript scriptLanguage="javascript" formName="loginForm" method="validateForm" staticJavascript="false" dynamicJavascript="true" page="0" />

<title>CCOS - Login Page</title>

</head>
<body style="background-image: url('image/login_bg.jpg')">
	<script language="JavaScript" type="text/JavaScript">
		function goToProcess(task) {
			
			if (!validateForm(document.forms[0])) {
				return;

			}
			
			document.forms[0].task.value = task;
			document.forms[0].submit();
		}
	</script>
	<div class="container">
		<div class="form-login">
			<html:form action="/login" method="post">
			<html:hidden name="loginForm" property="task" value="load" />
				<p>
					USER ID:<br/>
					<html:text property="user_id"/>
				</p>
				<p class="pass">
					PASSWORD:<br/>
					<html:password property="password"/>
				</p>
				<small><bean:write name="loginForm" property="error" /></small>
				<p class="buttons">
					<br>
					<a class="button" href="javascript:goToProcess('process')">Login</a>
				</p>
			</html:form>
		</div>
	</div>
</body>
</html>