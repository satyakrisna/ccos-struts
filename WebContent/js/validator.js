
// -------------------------------------------------------------------------- //
//   JavaScript to validate input data.                                       //
// -------------------------------------------------------------------------- //

/*

How To Use :
	<INPUT ....  onBlur="validateMe( this,'ALPH',10,2,0,0,1)" onFocus="setOriginalValue(this)">
	
Input parameter :
	- field : passing value from HTML tag/object.
	- type  : type of validation to be check such as :
			'NONE', 'NUM', 'STR', 'DATE', 'CURR',
			'DEC', 'ALPH', 'ALPHNUM', 'ZIP', 'PHONE', 'SSN',
			'EMAIL', 'URL', 'PERCENT', 'DOLLAR'.
	- length : maximum length of input in HTML page include precision.
	- precision 
	- lowerlimit 
	- upperlimit
	- inputRequired : 0/1 --> 0 = No Required input ; 1 = Required input.

DataType :
	* integer  - validateMe(this,'NUM',10,0,1,2147483647,1)
	* smallint - validateMe(this,'NUM',5,0,1,32767,1)
	* tinyint  - validateMe(this,'NUM',3,0,0,255,1)
	* Real (lower: -(3.4 x 10-38) ; upper : +(3.4 x 10+38)) - validateMe(this,'NUM',39,x,1,xxxx,0)
	* Float - (lower: -(1.7 x 10-308) ; upper : +(3.4 x 10+308)) - validateMe(this,'NUM',309,x,1,xxxx,0)
	* Money - validateMe(this,'CURR',20,2,1,922337203685477.5807,0)
			  validateMe(this,'DEC',20,x,1,922337203685477.5807,0)
	* SmallMoney -  validateMe(this,'CURR',11,2,1,214748.3647,0)
					validateMe(this,'DEC',11,x,1,214748.3647,0)
					
*/


var originalFieldValue;
var ginState = 0;
var gobjCurrent;

function setOriginalValue(field)
{	originalFieldValue = field.value;
}

function validateSPC(str)
{
	var A = new Array();
	A = str.split("\n");
	str = A.join("");
	A = str.split("\t");
	str = A.join("");
	A = str.split(" ");
	str = A.join("");
	return str;
}

function validateBlankSpace(field)
{
   var str = field.value;
   var strLength = str.length;
   var isBlank = false;
   if(strLength == 0)
     return false;
   else{
      for(i=0 ; i< strLength; i++)
	  {
	    if(str.charAt(i) == " ")
		  isBlank = false;
		else {
		  isBlank = true;
		  break
		}
	  }
   }
   return isBlank;
}

function validateBlankText(strInput)
{
   var str = strInput;
   var strLength = str.length;
   var isBlank = false;
   if(strLength == 0)
     return false;
   else{
      for(i=0 ; i< strLength; i++)
	  {
	    if(str.charAt(i) == " ")
		  isBlank = false;
		else {
		  isBlank = true;
		  break
		}
	  }
   }
   return isBlank;
}

function validateTEXT(str)
{
	var lentxt = validateSPC(str).length;
	if(lentxt != 0)
	{
		str = str + " ";
		lentxt = str.length;
		var A = "";
		var AX = new Array();
		var jumAX = 0;
		for(i=0;i<lentxt;i++)
		{
			if(str.charAt(i) == " ")
			{
				AX[jumAX] = A;
				A = "";
				jumAX++;
			}else
			{
				A = A + str.charAt(i);
			}
		}
		A = "";
		if(jumAX != 0)
		{
			for(i=0;i<jumAX;i++)
			{
				if(validateSPC(AX[i]).length != 0)
				{
					A = A + " "+AX[i];
				}
			}
		}
		str = A;
		return str;
	}
	return str;
}

function validSPACE(str)
{
	var lentxt = validateSPC(str).length;
	if(lentxt != 0)
	{
		lentxt = str.length;
		for(i=0;i<lentxt;i++)
		{
			if(str.charAt(i) == " ")
			{

				return false;
			}
		}
	}
	return true;
}

function validateNUM(str)
{
	var valisnum = "0123456789";
	str = validateSPC(str);
	var iclo = 0;
	for(i=0; i < str.length ; i++)
	{
		var tmpfc = str.charAt(i);
		if(valisnum.indexOf(tmpfc) == -1)
		{
			iclo++;
		}
	}
	if(iclo > 0)
	{
		return false;
	}else
	{
		return true;
	}
}

function validateMe(field, type, length, precision, lowerLimit, upperLimit, inputRequired)
{
	var gblerrorCode = false;
	var strUpperLimit = "";
	strUpperLimit    = upperLimit;
	var newValue = field.value;
	var thvalue  = validateTEXT(newValue);
	var newLength = 0;
	if(validateSPC(newValue).length != 0)
	{
		field.value = thvalue.substring(1,thvalue.length);
		newLength = newValue.length;
	} else
	{
		field.value = "";
		newLength = 0;
		return;
	}
	if(inputRequired == 0 && newLength == 0)
	{
		alert("blank");
		return
	}
	if (inputRequired == 1)
	{
		if (newLength == 0)
		{
			gblerrorCode = true;
		}else
		{
			gblerrorCode = false;
		}
	}else if(inputRequired == 2)
	{
		if (newLength == 0)
		{
			gblerrorCode = true;
		}else
		{
			if(validSPACE(field.value))
			{
				gblerrorCode = false;
			}else
			{
				gblerrorCode = true;
			}
		}
	}
	if (gblerrorCode == false)
	{
		if(type == 'NONE')
		{
		}

/*
type = 'NUM'
Objective : to check input numeric value.
*/
		else if(type == 'NUM')
		{
			var newLength = newValue.length
			for(var i = 0; i != newLength; i++)
			{
				aChar = newValue.substring(i,i+1)
				if(aChar < "0" || aChar > "9")
				{
					alert('This field requires a valid numeric value.');
					gblerrorCode = true;
					break;
				}
			}
			if(!gblerrorCode)
			{
				if(field.value < lowerLimit)
				{
					alert("The minimum value is "+lowerLimit);
					gblerrorCode = true;
				}else if (field.value > upperLimit)
				{
					alert("The maximum value is "+upperLimit);
					gblerrorCode = true;
				}
			}
			if(gblerrorCode)
			{
				field.value = "";
				field.focus();
				field.select();
			}
		}


/*
type = 'DATE'
Objective : to check date value.  It doesn't handle dates of the form mm.dd.yy (with periods) too well.
*/
		else if(type == 'DATE')
		{
			if( isNaN(Date.parse(field.value)) )
			{
				alert('This field requires a valid date.')
				gblerrorCode = true;
				field.value = "";
				field.focus();
				field.select();
			}
		}

/*
type = 'CURR'
Objective : to check Currency value. Precision is hard coded to 2, upper and lower limits are important.
Example : 12000 ---> 12000.00
*/
		else if(type == 'CURR')
		{
		   var newValue = field.value;
		   var newLength = newValue.length;
		   var newPrec = "";
		   var gstrNewStr = "";
		   var param =0;
		   linMod = ((newLength-3)%3)-1 ;

		   var nval = field.value;
		   var ADs = new Array();
		   var newDec = "";
		   var newCN = "";
		   ADs = nval.split(".");
		   var isck = false;
		   if(ADs.length > 2)
		   {
		      alert("Please enter with a correct currency number");
		      gstrNewStr = "";isck = false;
		   }else
		   {
		      if(ADs[0]) newDec = ADs[0]; else newDec = '0';
		      if(ADs[1]) newCN = ADs[1]; else newCN = '00';
		      newCN = validateSPC(newCN);
		      if(newCN.length == 0) newCN = '00';
		      if(validateSPC(newDec).length == 0) newDec = '0';
		      isck = false;
		      if((!validateNUM(newDec)) || (!validateNUM(newCN))) alert("Please enter a numeric value");
		      else if(newDec.length > length) alert("Maximum length of decimal value is "+length+" digit");
		      else if(parseFloat(newValue) < parseFloat(lowerLimit)) alert("The minimum value is "+parseFloat(lowerLimit));
		      else if(parseFloat(newValue) > parseFloat(upperLimit)) alert("The maximum value is "+parseFloat(upperLimit));
		      else if(newCN.length > precision) alert("Please enter "+precision+" digit of Decimal points");
		      else isck = true;
		      newCN = "."+ newCN;
		      newPrec = newCN;
		      gstrNewStr = newDec + " ";
		   }
		   field.value = gstrNewStr + newPrec;
		   if(isck == true)
		   {
		      field.value = validateSPC(field.value);
		   }else
		   {
		      field.value = "";
		      field.focus();
		   }

		}

/*
type = 'DEC'
Objective : to check Decimal value. Precision, upper and lower limits are important.
Example : 12000 ---> 12000.0..(x)
*/
		else if(type == 'DEC')
		{
			decimalValid(field, type, precision, lowerLimit, upperLimit)
		}
		
/*
type = 'TIME'
Objective : to check a valid Time Input.
Example a valid time : 00:00:00 - 23:59:59
*/
		else if (type == 'TIME')
		{
	    	var status1 = true
	        status1 = pictureValid(field, "##:##")
	        if(status1 == false )
	        {
				alert('This field requires a valid Time ##:## .')
	            gblerrorCode = true
	            field.value = ""
	            field.focus()
	            field.select()
			}
			else
			{
				newValue = field.value
				var hour = newValue.substring(0,2)
				var minute = newValue.substring(3,5)

				if((parseInt(hour) < 0)||(parseInt(hour) > 23))
				{
					alert('\"Hour\" must be equal or greater than 00 AND less than 24.')
					gblerrorCode = true
					field.value = ""
					field.focus()
					field.select()
				}
				if((parseInt(minute) < 0)||(parseInt(minute) > 59))
				{
					alert('\"Minute\" must be equal or greater than 00 AND less than 60.')
					gblerrorCode = true
					field.value = ""
					field.focus()
					field.select()
				}
		        }
			}

/*
type = 'ALPH'
Objective : to check input AlphaNumeric Value.
Characters allowed : "A...Za...z. -,"
*/
		    else if(type == 'ALPH')
		    {
				if(newLength > length)
				{
					alert('This field supports a maximum length of '+length.toString()+'.')
					field.value = ""
					field.focus()
					field.select()
				}
				for(var i = 0; i != newLength; i++)
				{
					aChar = newValue.substring(i,i+1)
					aChar = aChar.toUpperCase()
					if(aChar < "A" || aChar > "Z")
					{
						gblerrorCode = true
					}
				}
				if (gblerrorCode)
				{
					alert('This field requires a valid alphabetical string.')
					field.value = ""
					field.focus()
					field.select()
				}
			}

/*
	type='ALPHNUM'
	Objective : to check alpha numeric value
	Characters allowed : "0..9A..Za..z"
*/
		    else if(type == 'ALPHNUM')
			{
				if(newLength > length)
				{
					alert('This field supports a maximum length of '+length.toString()+'.')
					field.value = ""
					field.focus()
					field.select()
				}
				for(var i = 0; i != newLength; i++)
				{
					aChar = newValue.substring(i,i+1)
					aChar = aChar.toUpperCase()
					if( aChar < "0" || (aChar > "9" && aChar < "A") || aChar > "Z")
					{
						gblerrorCode = true
					}
				}
				if (gblerrorCode)
				{
					alert('This field requires a valid alphabetical numerical string.')
					field.value = ""
					field.focus()
					field.select()
				}
			}

/*
type = 'STR'
Objective : to check input String Value.
Characters allowed :  . -,0123456789:?><!@#$%^&*()_+=|}{][/\'\" a..zA..Z
*/
		    else if(type == 'STR')
			{
				   var extraChars=" -�������0123456789/&\:?!@,.*()_+";
		                var search = 0;

                if(newLength == 0)
                {
                        alert('This field requires a valid alphanumeric string.')
                        field.value = originalFieldValue
                        field.value = ""
                        field.focus()
                        field.select()
                }

						if(newLength == 0)
						{
							alert("Please fill this entry");
							field.value = "";
							field.focus();
							field.select();
						}
		                if(newLength > length)
		                {
		                        alert('This field supports a maximum length of '+length.toString()+'.');
		                        var vl_ = field.value;
		                        vl_ = vl_.substring(0,length);
		                        field.value = vl_;
		                        field.focus();
		                        field.select();
		                }
		                for(var i = 0; i != newLength; i++)
		                {
		                        aChar = newValue.substring(i,i+1)
		                        aChar = aChar.toUpperCase();
		                        search = extraChars.indexOf(aChar)
								if(aChar =="\\"){
									search=0;
								}else if(newValue.charCodeAt(i) == 13)
								{
									search = 0;
								}else if(newValue.charCodeAt(i) == 10)
								{
									search = 0;
								}
								
		                        if((search == -1) && (aChar < "A" || aChar > "Z") )
		                        {
		                            gblerrorCode = true;
		                        }
		                }
		                if (gblerrorCode == true)
		                {
							 alert('This field require a valid alphanumeric string.')
		                     field.value = "";
		                     field.focus();
		                     field.select();
		                }
        			}

/*
type = 'ZIP'
Objective : to check input ZIP Code with syntax ##### OR #####-####.
Characters allowed : Numeric and -
*/
			        else if(type == 'ZIP')
			        {
			                var status1 = true;
			                var status2 = true;

			                status1 = pictureValid(field, "#####");
			                if(status1 == false)
			                {
			                        status2 = pictureValid(field, "#####");
			                }

			                if(status1 == false && status2 == false)
			                {
								alert('This field requires a valid ZIP code of the form #####.');
			                    gblerrorCode = true;
			                    field.value = "";
			                    field.focus();
			                    field.select();
			                }
			        }

/*
type = 'PHONE'
Objective : to check PHONE Number syntax.
Allowed syntax : #######, ###-####,##########,###-###-####,###-###-####
Characters allowed : Numeric
*/
			        else if(type == 'PHONE' || type == 'FAX')
			        {
			                var extraChars="0123 456789+-()";
							var newValue = field.value;
							var newLength = newValue.length;
			                var search
			                if(newLength > length)
			                {
			                        alert('This field supports a maximum length of '+length.toString()+'.')
			                        field.value = field.value.substring(0,length-1)
			                        field.focus()
			                        field.select()
			                }

			                for(var i = 0; i != newLength; i++)
			                {
			                        aChar = newValue.substring(i,i+1)
			                        aChar = aChar.toUpperCase()
			                        search = extraChars.indexOf(aChar)
			                        if(search == -1)
			                        {
			                            gblerrorCode = true;
			                        }
			                }
			                if (gblerrorCode == true)
			                {

								 if (type=='PHONE')
								    alert('This field requires a valid phone number like : 021-1234567 .')
			                     else if (type=='FAX')
								    alert('This field requires a valid fax number like : 021-1234567 .')
			                     field.value = ""
			                     field.focus()
			                     field.select()
			                }
					}

			        else if(type == 'PHONE_')
			        {
			                var failed = true
			                var fieldtemp
			                var switchme = field.value

			                if(switchme.length == 7)
			                {
			                    if(pictureValid(field, "#######") == true)
			                    {
			                        fieldtemp = switchme.substring(0,3) + '-' + switchme.substring(3, switchme.length)
									field.value = fieldtemp
									failed = false;
			                    }
			                }
			                else if(switchme.length == 8)
			                {
			                   if(pictureValid(field, "###-####") == true)
			                   {
								failed = false;
			                   }
			                }
			                else if(switchme.length == 10)
			                {
			                   if(pictureValid(field, "##########") == true)
			                   {
			                      fieldtemp = switchme.substring(0,3) + '-' +
			                                  switchme.substring(3,6) + '-' +
			                                  switchme.substring(6, switchme.length);
			                      field.value = fieldtemp
							      failed = false;
			                   }
			                }
			                else if(switchme.length == 12)
			                {
			                   if(pictureValid(field, "###-###-####") == true)
			                   {
							      failed = false;
			                   }
							}

                			if(failed == true)
                			{
			                     alert('This field requires a valid phone number of the form ###-####, #######, ###-###-#### or ##########.')
				  				 gblerrorCode = true
			                     field.value = ""
			                     field.focus()
			                     field.select()
			                }
					}

			        else if(type == 'EXTPHONE')
			        {
			                var newLength = newValue.length
			                for(var i = 0; i != newLength; i++)
			                {
			                        aChar = newValue.substring(i,i+1)
			                        if(aChar < "0" || aChar > "9")
			                        {
			                            gblerrorCode = true
			                        }
			                }

			                if(gblerrorCode == true)
			                {
							    alert('This field requires a valid Extention Phone Number.')
			                    field.value = ""
			                    field.focus()
			                    field.select()
			                }
			        }

/*
Objective : to check SSN
Allowed syntax : ###-##-####,#########
*/
			        else if(type == 'SSN')
			        {
			                var status1 = true
			                var status2 = true
			                var fieldtemp

			                status1 = pictureValid(field, "###-##-####")
			                if(status1 == false)
			                {
			                        status2 = pictureValid(field, "#########")
			                        if(status2 == true)
			                        {
			                               fieldtemp = field.value.substring(0,3) + '-' + field.value.substring(3, 5) + '-' +  field.value.substring(5,9)
			                               field.value = fieldtemp
			                        }
			                }

			                if(status1 == false && status2 == false)
			                {
			                   alert('This field requires a valid Social Security number of the form ###-##-#### or #########.')
			                   gblerrorCode = true
			                   field.value = ""
			                   field.focus()
			                   field.select()
			                }
			        }

/*
type = 'EMAIL'
Objective : to check EMAIL syntax.
Wild Characters accepted : @-_
*/
			       	else if(type == 'EMAIL')
			       	{
						if (newLength > 5)
						{
							var linSumOfx = 0;
							var linSumOfy = 0
							for (j=0; j < newLength; j++)
							{
								Ch = newValue.charAt(j);

								if (ReplaceWildChar("@-_",Ch) == true)
								{
									gblerrorCode = true;
								}

								if (Ch == "@")
								{
									if (linSumOfy > 0)
									{
										linSumOfy = 0;
									}

									if ( newValue.substring(j,(j+2)) == "@.")
									{
										alert("Between \"@\" and \".\" should be one or more characters.");
				                        gblerrorCode = true;
				                        field.value =  ""
				                        field.focus()
					                    field.select()
									}
									linSumOfx++;
								}
								else if (Ch == ".")
								{
									linSumOfy++;
								}
							}

							if (gblerrorCode == true)
							{
								alert("There is a/some wild characters OR blank character.");
								gblerrorCode = true;
			                    field.value = ""
			                    field.focus()
			                    field.select()
							}

							else if ((linSumOfx > 1) || (linSumOfx < 1))
							{
								alert("Quantity of \"@\" should be 1.");
								gblerrorCode = true;
			                    field.value = ""
			                    field.focus()
			                    field.select()
							}
							else if (linSumOfy < 1)
							{
								alert("Quantity of \".\" should be more than or equal 1 after \" @\" .");
								gblerrorCode = true;
			                    field.value = ""
			                    field.focus()
			                    field.select()
							}
						}
						else
						{
							alert("Length of your email should be more than 5 characters.");
							gblerrorCode = true;
			                field.value = ""
			                field.focus()
			                field.select()
						}

			       	}

/*
Function : Validate Email without any alert message.
 			all error messages will be handle in pages.
*/
	   				else if(type == 'EMAIL_IFC')
			       	{

						if (newLength > 5)
						{
							var linSumOfx = 0;
							var linSumOfy = 0
							for (j=0; j < newLength; j++)
							{
								Ch = newValue.charAt(j);

								if (ReplaceWildChar("@-_",Ch) == true)
								{
									gblerrorCode = true;
								}

								if (Ch == "@")
								{
									if (linSumOfy > 0)
									{
										linSumOfy = 0;
									}

									if ( newValue.substring(j,(j+2)) == "@.")
									{
				                        gblerrorCode = true;
									}
									linSumOfx++;
								}
								else if (Ch == ".")
								{
									linSumOfy++;
								}
							}

							if (gblerrorCode == true)
							{
								gblerrorCode = true;
							}

							else if ((linSumOfx > 1) || (linSumOfx < 1))
							{
								gblerrorCode = true;
							}
							else if (linSumOfy < 1)
							{
								gblerrorCode = true;
							}
						}
						else
						{
							gblerrorCode = true;
						}

			       	}

/*
Function : To validate URL
Wild Characters accepted : -_:/
Example :    Input							Output
		"jakarta.co.id/book/"		----> "http://www.jakarta.co.id/book/"
		"www.jakarta.co.id"		    ----> "http://www.jakarta.co.id"
		"http://www.jakarta.co.id"  ----> "http://www.jakarta.co.id"
		Accept URL without www      ----> "http://ads3.zdnet.com/"
		Accept URL in IP format     ----> "http://172.19.10.90"
*/
		       else if(type == 'URL')
		       {
		  		  if (newValue.substring(0,11) == "http://www.")
				  {
						var lstrURL = "";
						var status = "0";
				  }
				  else if (newValue.substring(0,4) == "www.")
				  {
					var lstrURL = "http://";
					var status = "1";
				  }

				  else if (newValue.substring(0,7) == "http://")
				  {
						var lstrURL = "";
						var status = "0";
				  }
				  else
				  {
					var lstrURL = "http://";
					var status = "3";
				  }

				  var linSumOfDot = 0;
				  var linSumOfDotBeforeSlash = 0;
				  var linSumOfSlash = 0;
				  var linSumOfDot2 = 0;
				  var linSumOfSlash2 = 0;

					  for (j=0; j < newLength; j++)
					  {
							Ch = newValue.charAt(j);

							if (ReplaceWildChar("-_:/",Ch) == true)
							{
								gblerrorCode = true;
								field.value = "";
								field.focus();
								field.select();
							}

								if ((lstrURL == "http://www.") || (lstrURL == "http://"))
								{
									if ((Ch == ":") || (newValue.substring(j,j+1) == "//"))
									{
										gblerrorCode = true;
										field.value = "";
										field.focus();
										field.select();
									}
									if (Ch == ".")
									{
										if(j==0){
											gblerrorCode = true;
											field.value = "";
											field.focus();
											field.select();
										}
										else{
											linSumOfDot++;
										}
									}

									if (Ch == "/")
									{
										linSumOfSlash++;
										if ((lstrURL == "http://www.") || (lstrURL == "http://"))
										{
											if (linSumOfDot < 1)
											{
												gblerrorCode = true;
											}
										}
										else
										{
											if (linSumOfDot < 1)
											{
												gblerrorCode = true;
											}
										}

										if (linSumOfSlash == 1)
										{
											linSumOfDotBeforeSlash = linSumOfDot;
										}
									}
								}

								if (lstrURL == "")
								{
									if (Ch == ":")
									{
										linSumOfDot2++;
									}

									if ((Ch == ":") && (linSumOfDot2 > 1))
									{
										gblerrorCode = true;
									}

									if ((newValue.substring(j,j+1) == "//") && (linSumOfSlash2 > 1))
									{
										gblerrorCode = true
									}
									else
									{
										linSumOfSlash2++;
									}

									if (Ch == ".")
									{
										linSumOfDot++;
									}

									if ((Ch == "/") && (linSumOfSlash2 == 1))
									{
										if ((linSumOfDot < 2) && (linSumOfSlash == 0))
										{
											gblerrorCode = true;
										}
										else
										{
											linSumOfSlash++;
											if (linSumOfSlash == 1)
											{
												linSumOfDotBeforeSlash = linSumOfDot ;
											}
										}
									}
								}
						}
						if (linSumOfSlash == 0)
						{
							linSumOfDotBeforeSlash = linSumOfDot ;
						}

						if ((status <= "1"))
						{
							if (linSumOfDotBeforeSlash < 1)
							{
								gblerrorCode = true;
							}
						}
						else
						{
							if (linSumOfDotBeforeSlash < 1)
							{
								gblerrorCode = true;
							}
						}

						if (gblerrorCode == true)
						{
							alert('This field requires a valid syntax URL of the form .\nDomainName/  http://DomainName/  DomainName/ .');
							gblerrorCode = true
							field.value = "";
							field.focus();
							field.select();
						}
						else
						{
							field.value = lstrURL + newValue;
						}
		       }

	          else if(type == 'URL1')
       {

		 var onk = newValue.length;
		 var CountSpace = false;
		 var CekPointFirst = true;
   		 var tempstring = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		 var numeric = "1234567890";
		 var ValidUrl = true;
		 for(i=0;i<onk;i++)
		 {
			if (newValue.charAt(i) != " ") 		{
				CountSpace = true;
			}
		 	if (CountSpace)		{
				if (CekPointFirst)	{
					var first = newValue.charAt(i);
					CekPointFirst = false;
				}
				if (i == onk-1) {
					if (newValue.charAt(i) == ".")	{
						gblerrorCode = true;					}
				} else { 
					if (ValidUrl)	{
					 	if (i > 0)	{
							if (newValue.charAt(i) == ".")	{
								var markpre = newValue.charAt(i+1);
								if(tempstring.search(markpre) == -1)	{
									ValidUrl = false;
									gblerrorCode = true;
								}
							}
						}
					}
				}
			}
		 }

  		  if (newValue.substring(0,11) == "http://www.")
		  {
				var lstrURL = "";
				var status = "0";
		  }
		  else if (newValue.substring(0,4) == "www.")
		  {
			var lstrURL = "http://";
			var status = "1";
		  }
		  else if (newValue.substring(0,7) == "http://")
		  {
			var lstrURL = "";
			var status = "0";
		  }
		  else
		  {
			var lstrURL = "http://";
			var status = "3";
		  }

		  var linSumOfDot = 0;
		  var linSumOfDotBeforeSlash = 0;
		  var linSumOfSlash = 0;
		  var linSumOfDot2 = 0;
		  var linSumOfSlash2 = 0;

			  for (j=0; j < newLength; j++)
			  {
					Ch = newValue.charAt(j);

					if (ReplaceWildChar("-_:/",Ch) == true)
					{
						gblerrorCode = true;
						field.value = "";
						field.focus();
						field.select();
					}

						if ((lstrURL == "http://www.") || (lstrURL == "http://"))
						{
							if ((Ch == ":") || (newValue.substring(j,j+1) == "//"))
							{
								gblerrorCode = true;
								field.value = "";
								field.focus();
								field.select();
							}
							if (Ch == ".")
							{
								linSumOfDot++;
							}

							if (Ch == "/")
							{
								linSumOfSlash++;
								if (lstrURL == "http://www.")
								{
									if (linSumOfDot < 1)
									{
										gblerrorCode = true;
									}
								}
								else
								{
									if (linSumOfDot < 2)
									{
										gblerrorCode = true;
									}
								}

								if (linSumOfSlash == 1)
								{
									linSumOfDotBeforeSlash = linSumOfDot;
								}
							}
						}

						if (lstrURL == "")
						{
							if (Ch == ":")
							{
								linSumOfDot2++;
							}

							if ((Ch == ":") && (linSumOfDot2 > 1))
							{
								gblerrorCode = true;
							}

							if ((newValue.substring(j,j+1) == "//") && (linSumOfSlash2 > 1))
							{
								gblerrorCode = true
							}
							else
							{
								linSumOfSlash2++;
							}

							if (Ch == ".")
							{
								linSumOfDot++;
							}

							if ((Ch == "/") && (linSumOfSlash2 == 1))
							{
								if ((linSumOfDot < 2) && (linSumOfSlash == 0))
								{
									gblerrorCode = true;
								}
								else
								{
									linSumOfSlash++;
									if (linSumOfSlash == 1)
									{
										linSumOfDotBeforeSlash = linSumOfDot ;
									}
								}
							}

						}
				}
				// end of For statement

				if (linSumOfSlash == 0)
				{
					linSumOfDotBeforeSlash = linSumOfDot ;
				}

				if ((status <= "1"))
				{
					if (linSumOfDotBeforeSlash < 2)
					{
						gblerrorCode = true;
					}
				}
				else
				{
					if (linSumOfDotBeforeSlash < 1)
					{
						gblerrorCode = true;
					}
				}

				if (gblerrorCode == true)
				{
					alert('This field requires a valid syntax URL of the form .\nDomainName/  http://DomainName/  DomainName/ .');
					gblerrorCode = true
					field.value = "";
					field.focus();
					field.select();
				}
				else
				{
					field.value = lstrURL + newValue;
				}
       }

		       else if(type == 'FTP')
		       {
		  		  if (newValue.substring(0,10) == "ftp://www.")
				  {
						var lstrURL = "";
						var status = "0";
						newValue = newValue.substring(6,4);
				  }
				  else if (newValue.substring(0,4) == "www.")
				  {
					var lstrURL = "";
					var status = "1";

				  }
				  else if (newValue.substring(0,6) == "ftp://")
				  {
						var lstrURL = "";
						var status = "0";
						newValue = newValue.substring(6,1);
				  }
				  else
				  {
					var lstrURL = "";
					var status = "3";
				  }

				  var linSumOfDot = 0;
				  var linSumOfDotBeforeSlash = 0;
				  var linSumOfSlash = 0;
				  var linSumOfDot2 = 0;
				  var linSumOfSlash2 = 0;

					  for (j=0; j < newLength; j++)
					  {
							Ch = newValue.charAt(j);

							if (ReplaceWildChar("-_:/",Ch) == true)
							{
								gblerrorCode = true;
								field.value = "";
								field.focus();
								field.select();
							}

								if ((lstrURL == "ftp://www.") || (lstrURL == "ftp://"))
								{
									if ((Ch == ":") || (newValue.substring(j,j+1) == "//"))
									{
										gblerrorCode = true;
										field.value = "";
										field.focus();
										field.select();
									}
									if (Ch == ".")
									{
										if(j==0){
											gblerrorCode = true;
											field.value = "";
											field.focus();
											field.select();
										}
										else{
											linSumOfDot++;
										}
									}

									if (Ch == "/")
									{
										linSumOfSlash++;
										if ((lstrURL == "ftp://www.") || (lstrURL == "ftp://"))
										{
											if (linSumOfDot < 1)
											{
												gblerrorCode = true;
											}
										}
										else
										{
											if (linSumOfDot < 1)
											{
												gblerrorCode = true;
											}
										}

										if (linSumOfSlash == 1)
										{
											linSumOfDotBeforeSlash = linSumOfDot;
										}
									}
								}

								if (lstrURL == "")
								{
									if (Ch == ":")
									{
										gblerrorCode = true;
									}

									if ((Ch == ":") && (linSumOfDot2 > 1))
									{
										gblerrorCode = true;
									}

									if ((newValue.substring(j,j+1) == "//") && (linSumOfSlash2 > 1))
									{
										gblerrorCode = true
									}

									if (Ch == ".")
									{
										linSumOfDot++;
									}

									if ((Ch == "/") && (linSumOfSlash2 == 1))
									{
										if ((linSumOfDot < 2))
										{
											gblerrorCode = true;
										}
										else
										{
											linSumOfSlash++;
											if (linSumOfSlash == 1)
											{
												linSumOfDotBeforeSlash = linSumOfDot ;
											}
										}
									}
								}
						}

						if (linSumOfSlash == 0)
						{
							linSumOfDotBeforeSlash = linSumOfDot ;
						}

						if ((status <= "1"))
						{
							if (linSumOfDotBeforeSlash < 2)
							{
								gblerrorCode = true;
							}
						}
						else
						{
							if (linSumOfDotBeforeSlash < 2)
							{
								gblerrorCode = true;
							}
						}

						if (gblerrorCode == true)
						{
							alert('This field requires a valid syntax FTP of the form .\ne.q : 172.19.10.90 .');
							gblerrorCode = true
							field.value = "";
							field.focus();
							field.select();
						}
						else
						{
							field.value = lstrURL + newValue;
						}
		       }

/*
Function : To Validate Percentage (%) value.
A Valid Format for Input such as  ##.## % OR ##.##
Output : ##.## w/o %
Example : "2.25 %"  ---->  "2.25"
*/
       else if(type == 'PERCENT')
       {
			var lstrCheckOK = "1234567890.%";
			gstrNewStr = "";
			lblPct = false;

            if((field.value < lowerLimit) || (field.value > upperLimit))
            {
				gblerrorCode = true;
				field.value = ""
				field.focus()
				field.select()
            }

			CheckNumeric(newValue,"%.");
			if (allValid)
			{
				decimalValid(field, type, 3, lowerLimit, upperLimit)
				newValue = field.value;
				for (j=0; j < newValue.length ; j++)
				{
					lstrCh = newValue.charAt(j);
					for (k=0; k < lstrCheckOK.length ; k++)
					{
						if ((lstrCh == lstrCheckOK.charAt(k)) && (!lblPct))
						{
							if (lstrCh == ".")
							{
								gstrNewStr += lstrCh;
							}
							else if (lstrCh == "%")
							{
								lblPct = true;
								gstrNewStr == gstrNewStr;
							}
							else
							{
								gstrNewStr += lstrCh;
							}
						}
						else
						{
							k == lstrCheckOK.length;
						}
					}
				}
			}

			var gstrNewStr_ = parseFloat(gstrNewStr);
			if (gstrNewStr_ >= 0)
			{
				field.value = gstrNewStr;
			}
			else
			{
				gblerrorCode = true;
				field.value = "";
				field.focus();
				field.select();
			}
       } 

/*
Function : To Validate DOLLAR value.
*/	   
       else if (type == 'DOLLAR')
       {
	       	var lstrcheckOK = "0123456789.";
			var gstrNewStr = "";
            if((field.value < lowerLimit) || (field.value > upperLimit))
            {
				alert('This field requires a value in the range of ' + lowerLimit + ' to ' + upperLimit + '.')
				gblerrorCode = true;
				field.value = ""
				field.focus()
				field.select()
            }

			CheckNumeric(newValue,"$,.");

			if (allValid)
			{
				var lstrPoint = 0;
				var lstrPrecision = 0;

				for (var j=0; j < newValue.length ; j++)
				{
					lstrCh = newValue.charAt(j);

					for (var k=0; k < lstrcheckOK.length ; k++)
					{

						if (lstrCh == lstrcheckOK.charAt(k))
						{
							k=12;

							if (lstrCh == "."){
								if (lstrPoint < 1){
									lstrPoint++;
									gstrNewStr	= gstrNewStr + lstrCh;
								}
								else{
									gstrNewStr = gstrNewStr;
								}
							}

							else{
								if (lstrPoint==1){
									if(lstrPrecision < 2){
										lstrPrecision++;
										gstrNewStr = gstrNewStr + lstrCh;
									}
									else
									{
										gstrNewStr = gstrNewStr;
									}
								}

								else{
									gstrNewStr = gstrNewStr + lstrCh;
								}
							}
						}
					}
				}
			 }

			if(newValue =="$"){
				gstrNewStr = "0";
			}

			if(lstrPoint == 0){
				gstrNewStr = gstrNewStr + ".00";
			}
			if((lstrPoint==1)&&(lstrPrecision==0)){
				gstrNewStr = gstrNewStr + "00";
			}

			gstrNewStr = gstrNewStr;

			var gstrNewStr_ = parseFloat(gstrNewStr);

			if (gstrNewStr_ >=0)
			{
				field.value = gstrNewStr;
			}

			else
			{
				gblerrorCode = true;
				field.value = "";
				field.focus();
				field.select();
			}
       } 

}

else 
{
	if(inputRequired == 2)
	{
		 alert('There are no space allowed here');
    	 field.value = "";
	     field.focus();
	     field.select();
	}else
	{
		gblerrorCode = true;
	   field.value = "";
		field.focus();
		field.select();
	}
}
return (!gblerrorCode);

}

function decimalValid(field, type, precision, lowerLimit, upperLimit)
{
        var newValue = field.value
        var decAmount = ""
        var dolAmount = ""
        var decFlag = false
        var aChar = ""
        var totalValue = 0
        var decLen = 0

        for(i=0; i < newValue.length ; i++)
        {
	            aChar = newValue.substring(i,i+1)
                if(aChar >= "0" && aChar <= "9")
                {
                        if(decFlag)
                        {
                                decAmount = "" + decAmount + aChar
                        }
                        else
                        {
                                dolAmount = "" + dolAmount + aChar
                        }
                }
                else if(aChar == ".")
                {
                        if(decFlag)
                        {
                                dolAmount = ""
                                gblerrorCode = true
                                break
                        }
                        decFlag=true
                }
                else
                {
                        gblerrorCode = true
                        break
                }
        }

        if(dolAmount == "")
        {
                dolAmount = "0"
        }
        if(dolAmount.length > 1)
        {
                while(dolAmount.length > 1 && dolAmount.substring(0,1) == "0")
                {
                        dolAmount = dolAmount.substring(1,dolAmount.length)
                }
        }

        if(decAmount.length > precision)
        {
                if(decAmount.substring(precision,precision+1) > "4")
                {
                        var decAmount2 = parseInt(decAmount.substring(0,precision)) + 1

                        if(decAmount2 < Math.pow(10, precision-1))
                        {
                                decLen = precision - decAmount2.toString(10).length
                                for(var j=0; j< decLen; ++j)
                                {
                                        decAmount2 = "0" + decAmount2
                                }
                        }
                        else
                        {
                                decAmount2 = "" + decAmount2
                        }

                        decAmount = decAmount2
                }
                else
                {
                        decAmount = decAmount.substring(0,precision)
                }
                if (decAmount == Math.pow(10,precision))
                {
                        decAmount = ""
                        for(var j=0; j<= precision; ++j)
                        {
                                decAmount = "0" + decAmount
                        }

                        dolAmount = parseInt(dolAmount) + 1
                }
        }

        if(decAmount.length < precision)
        {
                decLen = precision-decAmount.length
                for(var j=0; j< decLen; ++j)
                {
                        decAmount = decAmount + "0"
                }
        }

        if (precision == 0) {
                totalValue =  dolAmount
        }
        else {
                totalValue = dolAmount + "." + decAmount
        }

        if( (parseFloat(totalValue) < lowerLimit) || (parseFloat(totalValue) > upperLimit) )
        {
                gblerrorCode = true
		}else
		{
			gblerrorCode = false;
        }

        if(gblerrorCode == true)
        {
                if(type == 'CURR')
                {
                        alert('This field requires a valid currency amount in the range of ' + lowerLimit + ' to ' + upperLimit + '.')
						return(false);
                }
                else
                {
                        alert('This field requires a valid decimal amount in the range of ' + lowerLimit + ' to ' + upperLimit + '.')
                }
                field.focus()
                field.select()
        }
        else
        {
                if(newValue.substring(0,1) != '-' || (dolAmount == "0" && decAmount == "00"))
                {
                        field.value = totalValue
                }
                else
                {
                        field.value = '-' + totalValue
                }
				return(true)
        }

}

//  PictureValid will validate against a format mask, such as (###) ###-####
// The following are valid values
// @ - Any character allowed by an HTML text element
// ? - Any Letter
// # - Any Number
// $ - Money Characters. 0 through 9, minus sign (-), plus sign (+), and decimal point (.)
// [] - Specifies custom data set to use for the character, such as [2345] would allow 2, 3, 4, or 5
// * - any number of the character that follows it.  For example, using an asterisk followed by a ?
//     would allow any number of letters to be entered.

// Globals used by pictureValid
var DIGITS = "0123456789"
var UPPERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
var LOWERS = "abcdefghijklmnopqrstuvwxyz"

function pictureValid(field, picture)
{
        var status = true
        var isRepeat = false
        var pLen = picture.length
        var search = 0
        var validData = ""
        var picChar = ""
        var aChar = ""
        var newValue = field.value
        var tLen = newValue.length
        var detailError = ""

        for(var i = 0, j = 0; (i != newValue.length)&& (j != picture.length)&& (status==true); i++)
        {
                picChar=picture.substring(j,j+1)
                if(picChar == "[")
                {
                        validData = ""
                        j++
                        for(; j != picture.length; j++)
                        {
                                if(picture.substring(j,j+1) == "]")
                                {
                                        break
                                }
                                validData = validData + picture.substring(j,j+1)
                        }
                }
                else if(picChar =="@")  // Any character
                {
                        j++
                        continue
                }
                else if(picChar == "?")   // Any letter
                {
                        validData = UPPERS + LOWERS
                }
                else if(picChar == "#")      // Any number
                {
                        validData = DIGITS
                }
                else if(picChar =="$")       // Money characters
                {
                        validData = DIGITS + "." + "-" + "+"
                }
                else if(picChar == "*")
                {
                        isRepeat = true
                        j++
                        i--
                        continue
                }

                else
                {
                        validData = picChar
                }

                aChar = newValue.substring(i,i+1)
                search = validData.indexOf(aChar)

                if(search == -1)
                {
                        if(isRepeat)
                        {
                                isRepeat = false
                                j++
                                i--
                                continue
                        }
                        status = false
                        if(aChar == " ")
                        {
                                detailError = "A space is not allowed in position #"+(i+1)+". "
                        }
                        else
                        {
                                detailError = "The character, " + aChar + ", is not allowed in position #"+(i+1)+". "
                        }
                }
                else
                {
                        if(!isRepeat)
                        {
                                j++
                        }
                }
       }

        //Check length
         if(status == true && (j < picture.length || i < newValue.length))
        {
                status = false
                detailError = "incorrect length"
        }

       return(status)
}

//Function strToCurr()
//Author: Greg Burgess
//Purpose: This function takes one parameter (a string representing a currency amount) which it validates
//formats and returns.
//
//Example strToCurr("12.5")   --> 12.50
//	  strToCurr("12")      --> 12.00
//	  strToCurr("12.59") --> 12.60

function strToCurr(newValue) {
        //var errorCode = false
        var decAmount = ""
        var dolAmount = ""
        var decFlag = false
        var aChar = ""
        var totalValue = 0
        var decLen = 0

        // ignore all but digits and decimal points.
        for(i=0; i < newValue.length; i++)
        {
                aChar = newValue.substring(i,i+1)
                if(aChar >= "0" && aChar <= "9")
                {
                        if(decFlag)
                        {
                                decAmount = "" + decAmount + aChar
                        }
                        else
                        {
                                dolAmount = "" + dolAmount + aChar
                        }
                }
                else if(aChar == ".")
                {
                        if(decFlag)
                        {
                                dolAmount = ""
                                gblerrorCode = true
                                break
                        }
                        decFlag=true
                }
                else
                {
                        gblerrorCode = true
                        break
                }
        }
        // Ensure that at least a zero appears for the dollar amount.
        if(dolAmount == "")
        {
                dolAmount = "0"
        }
        // Strip leading zeros.
        if(dolAmount.length > 1)
        {
                while(dolAmount.length > 1 && dolAmount.substring(0,1) == "0")
                {
                        dolAmount = dolAmount.substring(1,dolAmount.length)
                }
        }

        // Round the decimal amount.
        precision = 2;
        if(decAmount.length > precision)
        {
                if(decAmount.substring(precision,precision+1) > "4")
                {
                        var decAmount2 = parseInt(decAmount.substring(0,precision)) + 1

                        if(decAmount2 < Math.pow(10, precision-1))
                        {
                                decLen = precision - decAmount2.toString(10).length
                                for(var j=0; j< decLen; ++j)
                                {
                                        decAmount2 = "0" + decAmount2
                                }
                        }
                        else
                        {
                                decAmount2 = "" + decAmount2
                        }

                        decAmount = decAmount2
                }
                else
                {
                        decAmount = decAmount.substring(0,precision)
                }
                if (decAmount == Math.pow(10,precision))
                {
                        decAmount = ""
                        for(var j=0; j<= precision; ++j)
                        {
                                decAmount = "0" + decAmount
                        }

                        dolAmount = parseInt(dolAmount) + 1
                }
        }
        // Pad right side of decAmount
        if(decAmount.length < precision)
        {
                decLen = precision-decAmount.length
                for(var j=0; j< decLen; ++j)
                {
                        decAmount = decAmount + "0"
                }
        }

         totalValue = dolAmount + "." + decAmount
        if(gblerrorCode == true)
        {
                alert('This field requires a valid currency amount.')
	return 0
        }
        else
        {
	return totalValue
        }
}

/*
Function name: CheckNumeric(fobjToBeCheck, fstrParam)
Objective: check numeric value of string or inputted data on a html tag/object
Input parameter:
	fobjToBeCheck : String to be checked
	fstrParam : Some of Wild Character such as "%".
Output parameter : return value of variable "allValid" ---> true/false
*/

function CheckNumeric(fobjToBeCheck, fstrParam)
{
  var lstrCheckOK = "0123456789 " + fstrParam;
  allValid = true;
  for (i = 0;  i < fobjToBeCheck.length; i++)
  {
    lstrCh = fobjToBeCheck.charAt(i);

    for (j = 0; j < lstrCheckOK.length; j++)
      if (lstrCh == lstrCheckOK.charAt(j))
        break;

    if (j == lstrCheckOK.length)
    {
      allValid = false;
      break;
    }
  }

  if (!allValid)
  {
    alert("Please enter only valid characters in the field.");
    return (false);
  }
  return (true);
}

/**
 * Function name : CheckWildChar
 * Objective : Check wild char.
 * Input parameter :
 *      formField : field to cheked.
 * Return : true/false.
 */
function CheckWildChar(formField, lstrWildChar)
{
	var inputChar  = formField.value;
	if(lstrWildChar== "") {
	  lstrWildChar = "~!#$%^&*()+={}'\",<>@_:/|\\";

	}
	var isValid = true;
	if(inputChar == "")
	  return true;

	for(i = 0; i < inputChar.length; i++)
	{
	   lstrCh = inputChar.charAt(i);
	   for(j =0 ; j < lstrWildChar.length; j++)
	   {
		 if(lstrCh == lstrWildChar.charAt(j))
		 { isValid = false;
            break;
		 }
	   }
	   if(!isValid)
		  break;
	}
	if (!isValid)
    {
      alert("Please enter only valid characters in the field.");
	  formField.value="";
	  formField.focus();
      return (false);
    }
    return (true);
}

/*
Function name: ReplaceWildChar(fstrExclude,fstrCh)
Objective: check wild characters
Input parameter:
	fstrexclude : String to be checked
	fstrCh : Some of Wild Character such as "%" to be eliminate from list wild characters.
Output parameter : return value of variable "status" ---> true/false
*/

function ReplaceWildChar(fstrExclude,fstrCh)
{
	var lstrWildChar = "~!#$%^&*()+={}[]',<>?@-_:/|\ ";
	var status = false;
	fstrExclude = lstrWildChar.replace(fstrExclude,"");
    for (y = 0; y < fstrExclude.length; y++)
    {
      if (fstrCh == fstrExclude.charAt(y))
        {
			status = true;
			return(status);
        }
	}
	return(status);
}

/*
Function name: CheckWildChar(strCheck, strExclude)
Objective: check wild characters
Input parameter:
	strCheck : String to be checked
	strExclude : Some of Wild Character such as "%" to be eliminate from list wild characters.
Output parameter : return value of variable "status" ---> true/false
*/

function CheckWildChars(strCheck, strExclude)
{
	var lstrWildChar = "~!#$%^&*()+={}[]',<>?@-_:/|\"";
	var status = false;
	strExclude = lstrWildChar.replace(strExclude,"");
    for (y = 0; y < strExclude.length; y++)
    {
      if (strCheck.indexOf(strExclude.charAt(y)) > -1)
        {
			status = true;
			return(status);
        }
	}
	return(status);
}

/*
Function name: validateForm(gstrInputName)
Objective: check blank input (only required)
Input parameter:
	gstrInputName : Passing input text object must be not blank !
Output parameter : return value true/false.
*/

function validateForm(gstrInputName)
{
	var TempObject = "";
	var TempObj = new Array();
	var TempObject_ = "";
	var SumOfObject = 0;


	for(i=0;i<gstrInputName.length;i++){
		TempObject_ = gstrInputName.charAt(i);
		if( TempObject_ == ","){
			TempObj[SumOfObject] = TempObject;
			TempObject = "";
			SumOfObject++;
		}
		else{
			TempObject = TempObject + TempObject_;
		}
	}
	for(k=0;k<=(SumOfObject-1);k++){
		Temp = TempObj[k];
			if(document.forms[0].elements[Temp].value == ""){
				//alert('Input entries marked with a * must be entered.')
				document.forms[0].elements[Temp].focus();
				document.forms[0].elements[Temp].select();
				k=(SumOfObject-1);
				return false;
			}
		}
	return true;
}


function validateForms(gstrInputName, thisForm)
{
	var TempObject  = "";
	var TempObj     = new Array();
	var TempObject_ = "";
	var SumOfObject = 0;
    var status = false;

	for(i=0;i<gstrInputName.length;i++){
		TempObject_ = gstrInputName.charAt(i);
		if( TempObject_ == ","){
			TempObj[SumOfObject] = TempObject;
			TempObject = "";
			SumOfObject++;
		}
		else{
			TempObject = TempObject + TempObject_;
		}
	}

	for(k=0;k<=(SumOfObject-1);k++){
		    Temp = TempObj[k];
			if(thisForm.elements[Temp].value == ""){
			    thisForm.elements[Temp].focus();
				thisForm.elements[Temp].select();
				k=(SumOfObject-1);
				status= false;
			}
			else {
			  if(!validateBlankSpace(thisForm.elements[Temp]))
			  {
			      thisForm.elements[Temp].value = "";
			   	  thisForm.elements[Temp].focus();
				  thisForm.elements[Temp].select();
				  k=(SumOfObject-1);
				  status = false;
			  }
			  else {
				status =  true;
			  }
			}
		}
	return status;
}

/*
Function name: validateForm1(gstrInputName,IndexForm)
Objective: check blank input (only required) which use more than one FORM.
Input parameter:
	gstrInputName : Passing input text object must be not blank !
	IndexForm : number Form started from 0, counted form Top of HTML Page.
Output parameter : return value true/false.
*/

function validateForm1(gstrInputName,IndexForm)
{
	var TempObject = "";
	var TempObj = new Array();
	var TempObject_ = "";
	var SumOfObject = 0;

	for(i=0;i<gstrInputName.length;i++){
		TempObject_ = gstrInputName.charAt(i);
		if( TempObject_ == ","){
			TempObj[SumOfObject] = TempObject;
			TempObject = "";
			SumOfObject++;
		}
		else{
			TempObject = TempObject + TempObject_;
		}
	}
	for(k=0;k<=(SumOfObject-1);k++){
		Temp = TempObj[k];
			if(document.forms[IndexForm].elements[Temp].value == ""){
				alert('Input entries marked with a * must be entered.')
				document.forms[IndexForm].elements[Temp].focus();
				document.forms[IndexForm].elements[Temp].select();
				k=(SumOfObject-1)
				return false;
			}
		}
	return true;
}

/*
Function name: validateSE(gstrInputName,IndexForm)
Objective: Check Start Date and End Date.
Input parameter:
	gstrInputName : Passing input text object must be not blank !
	IndexForm : number Form started from 0, counted form Top of HTML Page.
Output parameter : return value true/false.
Example :
	input name = StartDate ;
	input name = EndDate ;
	validateSE('StartDate,EndDate,',0);
*/


function validateSE(gstrInputName,IndexForm){
	var TempObject = "";
	var TempObj = new Array();
	var TempObject_ = "";
	var SumOfObject = 0;

	for(i=0;i<gstrInputName.length;i++){
		TempObject_ = gstrInputName.charAt(i);
		if( TempObject_ == ","){
			TempObj[SumOfObject] = TempObject;
			TempObject = "";
			SumOfObject++;
		}
		else{
			TempObject = TempObject + TempObject_;
		}
	}
		Temp = TempObj[0];
		var StartDate = parseDate(document.forms[IndexForm].elements[Temp].value);
		Temp = TempObj[1];
		var EndDate = parseDate(document.forms[IndexForm].elements[Temp].value);

		if (Date.parse(StartDate) > Date.parse(EndDate)){
			alert('End Date must be equal or greater than Start Date')
			Temp = TempObj[0];
			document.forms[IndexForm].elements[Temp].focus();
			document.forms[IndexForm].elements[Temp].select();
			return false;
		}
}


function leftspace(sinput)
{
	var tmp="";
	var text1 = sinput;
	var i=0;
	while((i < text1.length) && (text1.substring(i,i+1) ==" "))
	{
			i++;
	}
	while(i<text1.length)
	{
		tmp=tmp+text1.substring(i,i+1);
		i++;
	}
	//alert("..");
	sinput.value=tmp;
	if(tmp.length > 0){
	}
	else{
		alert("data is empty");
		return false;
	}
}


//fungsi untuk menghilangkan space kanan
function rightspace(sinput)
{
	var tmp="";
	var text=sinput;
	var j=0;
	var i=text.length;
	while ((i>0) && (text.substring(i-1,i) == " "))
	{
		i--;
	}
	while(j<i)
	{
		tmp=tmp+text.substring(j,j+1);
		j++;
	}
	sinput.value=tmp;
	leftspace(tmp);
}
